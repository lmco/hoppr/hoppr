from typing import Any, AnyStr

from _typeshed import Incomplete
from typing_extensions import Self

basestring: Incomplete

class PackageURL:
    name: str
    namespace: str | None
    qualifiers: dict[str, str]
    subpath: str | None
    type: str
    version: str
    def __new__(cls, type: AnyStr | None = ..., namespace: AnyStr | None = ..., name: AnyStr | None = ..., version: AnyStr | None = ..., qualifiers: dict[str, str] = ..., subpath: AnyStr | None = ...) -> Self: ...
    def __hash__(self) -> int: ...
    def to_dict(self, encode: bool | None = ..., empty: Any = ...) -> dict[str, Any]: ...  # noqa: ANN401
    def to_string(self) -> str: ...
    @classmethod
    def from_string(cls, purl: str | None) -> PackageURL: ...
