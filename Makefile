MAKEFILE ?= ${abspath ${firstword ${MAKEFILE_LIST}}}

POETRY ?= poetry

TARGETS := hoppr test/unit

# ANSI color escape codes
BOLD ?= \033[1m
CYAN ?= \033[36m
GREEN ?= \033[32m
RED ?= \033[31m
YELLOW ?= \033[33m
NC ?= \033[0m # No Color

.PHONY: all build clean help format test
.SILENT: clean

#@ Tools
help: # Display this help
	@awk 'BEGIN {FS = ":.*#"; printf "\n${YELLOW}Usage: make <target>${NC}\n"} \
		/^[a-zA-Z_0-9-]+:.*?#/ { printf "  ${CYAN}%-15s${NC} %s\n", $$1, $$2 } \
		/^#@/ { printf "\n${BOLD}%s${NC}\n", substr($$0, 4) }' ${MAKEFILE} && echo

all: clean format test build

define poetry-run
	@${POETRY} run --ansi ${1}
endef

build: clean # Build hoppr distribution
	${POETRY} lock
	${POETRY} install --sync
	${POETRY} build

clean: # Clean the working directory
	${RM} -r dist .coverage* *.link .*.link-unfinished hopctl-merge-*.json
	find ${PWD} -type f -name "*.log" -exec ${RM} {} \;
	find ${PWD} -name "*.tar.gz" '(' -name "bundle*" -or -name "tarfile*" ')' -exec ${RM} {} \;
	find ${PWD} -name _delivered_bom.json -exec ${RM} {} \;

#@ Tests
test: format-check lint mypy pytest sourcery # Run format check, linting, type checks, and unit tests for all Python files

code-quality: # Create ruff CodeClimate report
	${call poetry-run,ruff check --output-file gl-code-quality-report.json || true}

format-check:
	${call poetry-run,ruff format --check}

lint: # Lint code with ruff
	${call poetry-run,ruff check --output-format grouped}

mypy: # Check static typing with mypy
	${call poetry-run,mypy}

pytest: # Run all unit tests
	${call poetry-run,pytest}

sourcery: # Check for refactoring suggestions with sourcery
	${call poetry-run,sourcery review --check --verbose ${TARGETS}}

#@ Formatting
format: format-fix lint-fix sourcery-fix # Format and refactor all Python files

format-fix: # Format python files with ruff
	${call poetry-run,ruff format}

lint-fix: # Lint code with ruff
	${call poetry-run,ruff check --fix --output-format grouped}

sourcery-fix: # Refactor python files with sourcery
	${call poetry-run,sourcery review --fix --verbose ${TARGETS}}
