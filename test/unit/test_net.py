"""Test module for net class."""

from __future__ import annotations

import json

from typing import TYPE_CHECKING

import pytest

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr import cdx
from hoppr.exceptions import HopprLoadDataError
from hoppr.models.credentials import CredentialRequiredService
from hoppr.net import Credentials, requests

if TYPE_CHECKING:
    from collections.abc import Callable
    from pathlib import Path


test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}


@pytest.mark.parametrize(
    argnames=["algorithm", "expected_hash"],
    argvalues=[
        pytest.param(
            "blake2b",
            "443e7aa109dd46978a4ab865dbda0cbe3cfcead7e890f8a3347079b908e3ec6f"
            "bc2092a4969f7a53ee15da686d9ddb3715a7302a1a66f64b13e4278d271e04b8",
            id="BLAKE2B",
        ),
        pytest.param(
            "blake2s",
            "89769579bb89df6e875b81e0f10320946796ea0eaa150c6c634c8f5f409bf55b",
            id="BLAKE2S",
        ),
        pytest.param(
            "md5",
            "d8d4c2d1c8e4b04d96bca23175d071c5",
            id="MD5",
        ),
        pytest.param(
            "sha1",
            "381e08efd0d8182d2a559321b2b60234010f74bc",
            id="SHA1",
        ),
        pytest.param(
            "sha224",
            "ba3b6dad72628e410bba30558503d850253b3efa250690bf3e91cf1e",
            id="SHA224",
        ),
        pytest.param(
            "sha256",
            "bf79be0c21a100565100d16b31deee78ce5391f66c0774405d484ce38b6076e0",
            id="SHA256",
        ),
        pytest.param(
            "sha384",
            "1a380fe6b1ce001c5d2ae4e57c23c0249ca6d2ee875ee7ed611976e4423994b059e5d29c26f8fa73d0c60873c0abc637",
            id="SHA384",
        ),
        pytest.param(
            "sha512",
            "ea70fa7712fb3e378c18ee1026bfe79bb9b099894b3dff868c5015051ec8363e"
            "c29f66f39f392962e1fc619bebcf89d8c6e334c387f6a6701e8d475794cb823b",
            id="SHA512",
        ),
        pytest.param(
            cdx.HashAlg.BLAKE2B_512,
            "443e7aa109dd46978a4ab865dbda0cbe3cfcead7e890f8a3347079b908e3ec6f"
            "bc2092a4969f7a53ee15da686d9ddb3715a7302a1a66f64b13e4278d271e04b8",
            id="cdx.HashAlg.BLAKE2B_512",
        ),
        pytest.param(
            cdx.HashAlg.BLAKE2b_512,
            "443e7aa109dd46978a4ab865dbda0cbe3cfcead7e890f8a3347079b908e3ec6f"
            "bc2092a4969f7a53ee15da686d9ddb3715a7302a1a66f64b13e4278d271e04b8",
            id="cdx.HashAlg.BLAKE2b_512",
        ),
        pytest.param(
            cdx.HashAlg.BLAKE2B_256,
            "12e772b9895a6e2dbbe5e8e222905d9e27b564bca0d2992dc1977f2690092795",
            id="cdx.HashAlg.BLAKE2B_256",
        ),
        pytest.param(
            cdx.HashAlg.BLAKE2b_256,
            "12e772b9895a6e2dbbe5e8e222905d9e27b564bca0d2992dc1977f2690092795",
            id="cdx.HashAlg.BLAKE2b_256",
        ),
        pytest.param(
            cdx.HashAlg.MD5,
            "d8d4c2d1c8e4b04d96bca23175d071c5",
            id="cdx.HashAlg.MD5",
        ),
        pytest.param(
            cdx.HashAlg.SHA_1,
            "381e08efd0d8182d2a559321b2b60234010f74bc",
            id="cdx.HashAlg.SHA_1",
        ),
        pytest.param(
            cdx.HashAlg.SHA_256,
            "bf79be0c21a100565100d16b31deee78ce5391f66c0774405d484ce38b6076e0",
            id="cdx.HashAlg.SHA_256",
        ),
        pytest.param(
            cdx.HashAlg.SHA_384,
            "1a380fe6b1ce001c5d2ae4e57c23c0249ca6d2ee875ee7ed611976e4423994b059e5d29c26f8fa73d0c60873c0abc637",
            id="cdx.HashAlg.SHA_384",
        ),
        pytest.param(
            cdx.HashAlg.SHA_512,
            "ea70fa7712fb3e378c18ee1026bfe79bb9b099894b3dff868c5015051ec8363e"
            "c29f66f39f392962e1fc619bebcf89d8c6e334c387f6a6701e8d475794cb823b",
            id="cdx.HashAlg.SHA_512",
        ),
    ],
)
def test_get_file_hash(algorithm: hoppr.cdx.HashAlg | hoppr.net.HashlibAlgs, expected_hash: str, tmp_path: Path):
    """Test hoppr.net.get_file_hash function."""
    artifact = tmp_path / "test-artifact.txt"
    artifact.write_bytes(b"0" * 1048576)

    assert hoppr.net.get_file_hash(artifact, algorithm) == expected_hash


@pytest.mark.parametrize(
    argnames=["algorithm", "expected_hash"],
    argvalues=[
        pytest.param(
            cdx.HashAlg.BLAKE2B_384,
            "????????????????????????????????????????",
            id="cdx.HashAlg.BLAKE2B_384",
        ),
        pytest.param(
            cdx.HashAlg.BLAKE3,
            "????????????????????????????????????????",
            id="cdx.HashAlg.BLAKE3",
        ),
    ],
)
def test_get_file_hash_unsupported_algorithm(
    algorithm: hoppr.cdx.HashAlg | hoppr.net.HashlibAlgs, expected_hash: str, tmp_path: Path
):
    """Test hoppr.net.get_file_hash function."""
    artifact = tmp_path / "test-artifact.txt"
    artifact.write_bytes(b"0" * 1048576)

    with pytest.raises(ValueError):  # noqa: PT011
        hoppr.net.get_file_hash(artifact, algorithm)


def test_load_url(
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test hoppr.net.load_url function."""
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": test_input_data}], indirect=True)
def test_load_url_unsupported_input_type(
    monkeypatch: pytest.MonkeyPatch, response_fixture: Callable[[str], requests.Response]
):
    """Test hoppr.net.load_url function failure due to unsupported input."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"content": json.dumps(test_input_data).encode("utf-8")}], indirect=True
)
def test_load_url_bytes(monkeypatch: pytest.MonkeyPatch, response_fixture: Callable[[str], requests.Response]):
    """Test hoppr.net.load_url function with byte data."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": "BAD DATA"}], indirect=True)
def test_load_url_bad_data(monkeypatch: pytest.MonkeyPatch, response_fixture: Callable[[str], requests.Response]):
    """Test hoppr.net.load_url function raises exception given bad data."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"0" * 1024}], indirect=True)
def test_download_file(
    monkeypatch: pytest.MonkeyPatch,
    cred_object_fixture: CredentialRequiredService,
    find_credentials_fixture: Callable[[str], CredentialRequiredService],
    response_fixture: Callable[[str], requests.Response],
    tmp_path: Path,
):
    """Test hoppr.net.download_file function."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    download_path = tmp_path / "test_net::test_download_file"
    response = hoppr.net.download_file(url=cred_object_fixture.url, dest=str(download_path))

    assert response.status_code == 200
    assert download_path.is_file()
    assert download_path.read_bytes() == b"0" * 1024


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"0" * 1024}], indirect=True)
def test_download_file_without_credentials(
    monkeypatch: pytest.MonkeyPatch, response_fixture: Callable[[str], requests.Response], tmp_path: Path
):
    """Test hoppr.net.download_file function without credentials."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    download_path = tmp_path / "test_net::test_download_file"
    response = hoppr.net.download_file(url="http://test.hoppr.com", dest=str(download_path))

    assert response.status_code == 200
    assert download_path.is_file()
    assert download_path.read_bytes() == b"0" * 1024
