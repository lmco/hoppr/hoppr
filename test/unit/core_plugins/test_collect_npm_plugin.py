"""Test module for CollectNpmPlugin class."""

from __future__ import annotations

from collections.abc import Callable
from pathlib import Path

import pytest

import hoppr.plugin_utils

from hoppr import cdx, net
from hoppr.core_plugins.collect_npm_plugin import CollectNpmPlugin, requests
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component, ComponentType, Hash
from hoppr.models.types import PurlType, RepositoryUrl
from hoppr.result import Result, ResultStatus


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """Return test_comp fixture."""
    return Component(
        name="TestComponent",
        purl="pkg:npm/async@1.5.2",
        type=ComponentType.FILE,
    )


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectNpmPlugin, monkeypatch: pytest.MonkeyPatch, tmp_path: Path
) -> CollectNpmPlugin:
    """Override and parametrize plugin_fixture to return CollectNpmPlugin."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://url1.com"])

    plugin_fixture.context.repositories[PurlType.NPM] = [
        Repository.parse_obj({"url": "https://url2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["response_generator", "component_hashes", "component_file_hash", "expected_result"],
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"versions":{"1.5.2":{"dist":{"integrity":"sha512-oKMrFZ/spJ57E7mkmuASet5Yf4s="}}}}',
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.SUCCESS,
            id="no_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"versions":{"1.5.2":{"dist":{"integrity":"sha512-oKMrFZ/spJ57E7mkmuASet5Yf4s="}}}}',
                },
            ],
            [
                Hash(alg=cdx.HashAlg.MD5, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_1, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_384, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_384, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
            ],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.SUCCESS,
            id="correct_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"versions":{"1.5.2":{"dist":{"integrity":"sha512-oKMrFZ/spJ57E7mkmuASet5Yf4s="}}}}',
                },
            ],
            [Hash(alg=cdx.HashAlg.SHA_1, content="0000000000000000000000000000000000000000")],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="incorrect_sbom_hash",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"versions":{"1.5.2":{"dist":{"integrity":"sha512-oKMrFZ/spJ57E7mkmuASet5Yf4s="}}}}',
                },
            ],
            [],
            "0000000000000000000000000000000000000000",
            ResultStatus.FAIL,
            id="incorrect_file_hash",
        ),
        pytest.param(
            [
                {"status_code": 500, "content": b"0" * 1024},
                {
                    "status_code": 500,
                    "content": b"",
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="error_downloading_checksum",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"banana": "ba na na"}}',
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="metadata_missing_checksum",
        ),
    ],
    indirect=["response_generator"],
)
def test_collect_npm(
    plugin_fixture: CollectNpmPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_generator: Callable[..., requests.Response],
    component: Component,
    component_hashes: list[Hash],
    component_file_hash: str,
    expected_result: ResultStatus,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test npm collector successful run with a given url."""
    component.hashes = component_hashes

    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_generator)
        patch.setattr(target=net, name="get_file_hash", value=lambda alg, path: component_file_hash)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.status == expected_result


RETRY_MESSAGE_TEMPLATE = "Failed to download NPM package for {purl}, status_code={status_code}"


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_return_status", "expected_msg_template"],
    argvalues=[
        ({"status_code": 401}, 401, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 403}, 403, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 404}, 404, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 500}, 500, RETRY_MESSAGE_TEMPLATE),
        ({"status_code": 502}, 502, RETRY_MESSAGE_TEMPLATE),
    ],
    ids=[
        "Unauthorized (401)",
        "Forbidden (403)",
        "Not Found (404)",
        "Internal Server Error (500)",
        "Bad Gateway (502)",
    ],
    indirect=["response_fixture"],
)
def test_collect_npm_fail(
    plugin_fixture: CollectNpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    expected_msg_template: str,
    expected_return_status: int,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test npm collector unsuccessful run with a given url."""
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.endswith(
            expected_msg_template.format(purl=component.purl, status_code=expected_return_status)
        )


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": b'{"versions":{"1.5.2":{"dist":{"integrity":"sha512-oKMrFZ/spJ57E7mkmuASet5Yf4s="}}}}'},
            id="stream-success",
        ),
        pytest.param(
            "Failed to retrieve data from https://url1.com/async/-/async-1.5.2.tgz",
            {"status_code": 404},
            id="stream-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test__stream_url_data(
    expected_exception: str | None,
    plugin_fixture: CollectNpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectNpmPlugin._stream_url_data method."""
    comp_url = "https://url1.com/async/-/async-1.5.2.tgz"

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        response = plugin_fixture._stream_url_data(url=RepositoryUrl(url=comp_url))
        assert response.url == comp_url
    except requests.HTTPError as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture", "expected_hash"],
    argvalues=[
        pytest.param(
            "Failed to download NPM package metadata from https://url1.com/async.",
            {"status_code": 404},
            None,
            id="stream_failed",
        ),
        pytest.param(
            "Package metadata is missing package checksum.",
            {"content": b'{"versions":{"1.5.2":{"dist":{"integrity": ""}}}}'},
            None,
            id="missing_checksum",
        ),
    ],
    indirect=["response_fixture"],
)
def test__get_package_checksum(
    expected_exception: str | None,
    expected_hash: str | None,
    plugin_fixture: CollectNpmPlugin,
    response_fixture: Callable[..., requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """_summary_."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        npm_metadata_response = plugin_fixture._get_package_checksum(
            RepositoryUrl(url="https://url1.com/async/"), "1.5.2", None
        )
        assert npm_metadata_response == expected_hash
    except (HopprPluginError, ValueError) as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture", "expected_hash"],
    argvalues=[
        pytest.param(
            "Failed to download NPM package metadata from https://url1.com/async.",
            {"status_code": 404},
            None,
            id="no_metadata",
        )
    ],
    indirect=["response_fixture"],
)
def test__validate_hashes(
    expected_exception: str | None,
    expected_hash: str | None,
    plugin_fixture: CollectNpmPlugin,
    component: Component,
    response_fixture: Callable[..., requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectNpmPlugin._validate_hashes method."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        validation_hash = plugin_fixture._validate_hashes(
            component, RepositoryUrl(url="https://url1.com/async/"), "1.5.2", None
        )
        assert validation_hash == expected_hash
    except (HopprPluginError, ValueError) as ex:
        assert str(ex) == expected_exception  # noqa: PT017
