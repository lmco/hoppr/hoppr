"""Test module for CollectGolangPlugin class."""

from __future__ import annotations

import zipfile

from collections.abc import Callable
from pathlib import Path

import hoppr_cyclonedx_models.cyclonedx_1_5 as cdx
import pytest
import requests

import hoppr.plugin_utils

from hoppr import net
from hoppr.core_plugins.collect_golang_plugin import CollectGolangPlugin
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component, Hash
from hoppr.models.types import PurlType
from hoppr.result import Result, ResultStatus


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """Test Component fixture."""
    return Component(
        name="TestComponent",
        purl="pkg:golang/example/package@1.2.3",
        type="file",  # type: ignore[arg-type]
    )


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectGolangPlugin, monkeypatch: pytest.MonkeyPatch, tmp_path: Path
) -> CollectGolangPlugin:
    """Override and parametrize plugin_fixture to return CollectGolangPlugin."""
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=Result.success,
    )
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_repos",
        value=lambda comp: ["https://somewhere1.com"],
    )

    plugin_fixture.context.repositories[PurlType.GOLANG] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=[
        "response_generator",
        "component_hashes",
        "component_file_hash",
        "expected_result",
    ],
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    golang/example/package v1.2.3 h1:ofbhZ5ZY9AjHATWQie4qd2JfncdUmvcSA/zfQB767Dk=""",
                },
            ],
            [],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.SUCCESS,
            id="no_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    golang/example/package v1.2.3 h1:ofbhZ5ZY9AjHATWQie4qd2JfncdUmvcSA/zfQB767Dk=""",
                },
            ],
            [
                Hash(
                    alg=cdx.HashAlg.MD5,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA_1,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA_256,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA_384,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA_512,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA3_512,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA3_256,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
                Hash(
                    alg=cdx.HashAlg.SHA3_384,
                    content="a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
                ),
            ],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.SUCCESS,
            id="correct_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    golang/example/package v1.2.3 h1:ofbhZ5ZY9AjHATWQie4qd2JfncdUmvcSA/zfQB767Dk=""",
                },
            ],
            [
                Hash(
                    alg=cdx.HashAlg.SHA_1,
                    content="0000000000000000000000000000000000000000",
                )
            ],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.FAIL,
            id="incorrect_sbom_hash",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    golang/example/package v1.2.3 h1:ETg8tcj4OhrB84UEgeE8dSuV/0h4BBL1uOV/qK0vlyI=""",
                },
            ],
            [],
            "0000000000000000000000000000000000000000",
            ResultStatus.FAIL,
            id="incorrect_file_hash",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 500,
                    "content": b"",
                },
            ],
            [],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.FAIL,
            id="error_downloading_checksum",
        ),
        pytest.param(
            [
                {"status_code": 500, "content": b""},
                {
                    "status_code": 500,
                    "content": b"",
                },
            ],
            [],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.FAIL,
            id="error_downloading_checksum",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    github.com/xanzy/go-gitlab v0.100.0 h1:ofbhZ5ZY9AjHATWQie4qd2JfncdUmvcSA/zfQB767Dk=""",
                },
            ],
            [],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.FAIL,
            id="metadata_incorrect_name_version",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b"""23517647
                    github.com/xanzy/go-gitlab v0.100.0 h1:""",
                },
            ],
            [],
            "a1f6e1679658f408c701359089ee2a77625f9dc7549af71203fcdf401efaec39",
            ResultStatus.FAIL,
            id="metadata_missing_checksum",
        ),
    ],
    indirect=["response_generator"],
)
def test_collect_golang(
    plugin_fixture: CollectGolangPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_generator: Callable[[str], requests.Response],
    component: Component,
    component_hashes: list[Hash],
    component_file_hash: str,
    expected_result: ResultStatus,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test golang collector run with a given url."""
    component.hashes = component_hashes

    def _go_hash_patch(artifact: str):
        return Hash(alg=cdx.HashAlg.SHA_256, content=component_file_hash)

    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_generator)
        patch.setattr(
            target=net,
            name="get_file_hash",
            value=lambda path, alg: component_file_hash,
        )
        patch.setattr(target=plugin_fixture, name="_get_golang_file_hash", value=_go_hash_patch)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.status == expected_result


@pytest.mark.parametrize(
    argnames=["should_error", "is_zip", "files"],
    argvalues=[
        pytest.param(
            True,
            False,
            [],
            id="not_a_zip_file",
        ),
        pytest.param(
            True,
            True,
            ["this_file.go", "that_file.go\n"],
            id="invalid_file_name",
        ),
        pytest.param(
            False,
            True,
            ["this_file.go", "that_file.go"],
            id="correct",
        ),
    ],
)
def test_get_golang_file_hash(
    plugin_fixture: CollectGolangPlugin,
    should_error: bool,
    is_zip: bool,
    files: list[str],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test golang collector file hashing method."""

    class MockZip:
        """Mock zipfile.ZipFile Class."""

        file_content = b"""
        package main
        import (
                "fmt"
                dirhash 'golang.org/x/mod/sumdb/dirhash'
                )
        func main() {
            str, err := dirhash.HashZip('go-gitlab.zip', dirhash.Hash1)
            if err != nil {
                fmt.Println(err.Error())
            }
            fmt.Println(str)
        }
        """

        def __init__(self, file: str):
            pass

        def __enter__(self):  # noqa: ANN204
            return self

        def namelist(self):
            """Mock namelist function. Returns files in zip."""
            return files

        def sort(self):
            """Mock sort function."""

        def read(self, filename: str):
            """Mock fileread function."""
            return self.file_content

        def __exit__(self, t, v, tb):  # noqa: ANN001
            pass

    correct_hash = Hash(
        alg=cdx.HashAlg.SHA_256,
        content="655c341e83f6d7f8e6893cd68613c2d593ccf13c4ab4cf5efa7acaa99c7235a5",
    )

    with monkeypatch.context() as patch:
        patch.setattr(target=zipfile, name="is_zipfile", value=lambda x: is_zip)
        patch.setattr(target=zipfile, name="ZipFile", value=MockZip)

        if should_error:
            with pytest.raises(HopprPluginError):
                test_hash = plugin_fixture._get_golang_file_hash("test.zip")

        else:
            test_hash = plugin_fixture._get_golang_file_hash("test.zip")

            assert test_hash == correct_hash


def test_get_version(plugin_fixture: CollectGolangPlugin):
    """Test golang collector has version."""
    assert len(plugin_fixture.get_version()) > 0
