"""Test module for CollectDockerPlugin class."""

from __future__ import annotations

import logging
import re

from collections.abc import Callable
from os import PathLike
from pathlib import Path
from subprocess import CompletedProcess

import pytest

from oras.container import Container
from oras.provider import Registry
from packageurl import PackageURL
from pydantic import SecretStr
from requests import Response

import hoppr.plugin_utils
import hoppr.utils

from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.sbom import Component
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture(request: pytest.FixtureRequest) -> Component:
    """Test Component fixture."""
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get("purl", "pkg:docker/something/else@1.2.3")
    param_dict["type"] = param_dict.get("type", "file")

    return Component(**param_dict)


def _mock_get_repos(comp: Component) -> list[str]:
    """Mock _get_repos method."""
    return ["http://somewhere.com", "https://somewhere.com"]


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "component", "source_image", "directory_string"],
    argvalues=[
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@1.2.3"},
            "docker://somewhere.com/something/else:1.2.3",
            "somewhere.com/something/else@1.2.3",
            id="semantic",
        ),
        pytest.param(
            {"returncode": 0},
            {
                "purl": "pkg:docker/something/else@sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00"
            },
            "docker://somewhere.com/something/else@sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            "somewhere.com/something/else@sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            id="sha256",
        ),
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1"},
            "docker://somewhere.com/something/else@sha256:40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
            "somewhere.com/something/else@sha256%3A40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
            id="hash",
        ),
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@latest?repository_url=docker.io"},
            "docker://somewhere.com/something/else:latest",
            "somewhere.com/something/else@latest",
            id="qualifier",
        ),
    ],
    indirect=["completed_process_fixture", "component"],
)
def test_collect_docker_success(
    plugin_fixture: CollectDockerPlugin,
    component: Component,
    source_image: str,
    directory_string: str,
    completed_process_fixture: CompletedProcess,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test a successful run of the Docker Collector."""
    already_run = False

    def _run_command(
        command: list[str],
        password_list: list[str] | None = None,
        cwd: str | PathLike[str] | None = None,
    ) -> CompletedProcess[bytes]:
        nonlocal already_run
        if already_run:
            pytest.fail(reason="expected `run_command` to be called exactly once")

        already_run = True
        purl = hoppr.utils.get_package_url(component.purl)

        assert command == [
            "skopeo",
            "copy",
            "--additional-tag",
            f"{purl.name}:{purl.version.replace(':', '')}",
            "--src-tls-verify=false",
            "--debug",
            source_image,
            f"docker-archive:{plugin_fixture.context.collect_root_dir}/docker/http%3A%2F%2F{directory_string}",
        ]
        assert password_list == []
        assert cwd is None

        return completed_process_fixture

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="_get_digest", value=lambda *_, **__: "sha256:somesha")
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=_run_command)

    plugin_fixture.get_logger().setLevel(logging.DEBUG)

    purl = hoppr.utils.get_package_url(component.purl)
    (
        (plugin_fixture._get_target_path("http://somewhere.com", purl)).parent / f"{purl.name}@{purl.version}.json"
    ).write_text("{Test json}")
    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    # Validate that url pattern matches regex.
    # Regex docker://word
    pattern = re.compile(r"^docker://\w+.*")
    purl = hoppr.utils.get_package_url(component.purl)
    src_image = plugin_fixture._get_image(url="https://docker.io", purl=purl)
    matched = bool(pattern.match(src_image.url))
    assert matched


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_docker_fail(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: pytest.MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """Test a failing run of the Docker Collector."""
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=Path, name="exists", value=lambda *_, **__: True)
    monkeypatch.setattr(target=Path, name="unlink", value=lambda *_, **__: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Skopeo failed to copy docker image"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"skopeo_command": "skopeo"}], indirect=True)
def test_collect_docker_command_not_found(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: pytest.MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """Test if the required command is not found."""
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=lambda message: Result.fail("[mock] command not found"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


@pytest.fixture(name="oci_component")
def oci_component_fixture(request: pytest.FixtureRequest) -> Component:
    """Test Oci Component fixture."""
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get(
        "purl",
        "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest",
    )
    param_dict["type"] = param_dict.get("type", "file")
    param_dict["qualifiers"] = param_dict.get("repository_url", "somewhere.com")

    return Component(**param_dict)


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 0, "stdout": bytes("sha256:somesha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_success(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test a successful run of the Docker Collector with an OCI package."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_digest", value=lambda *_, **__: "sha256:somesha")

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 0, "stdout": bytes("sha256:differentsha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_fail_tag_mismatch(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test a failure run of the Docker Collector with an OCI package <tag mismatch>."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Provided tag 'latest' image digest does not match 'sha256:somesha'")


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "oci_component"],
    argvalues=[
        pytest.param(
            {"returncode": 1, "stdout": bytes("sha256:somesha", "utf-8")},
            {
                "purl": "pkg:oci/test-component@sha256%3Asomesha?repository_url=somewhere.com/something/test-component&tag=latest"
            },
        ),
    ],
    indirect=["completed_process_fixture", "oci_component"],
)
def test_collect_oci_fail_no_image_digest(
    plugin_fixture: CollectDockerPlugin,
    oci_component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test a successful run of the Docker Collector with an OCI package."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=lambda *_, **__: completed_process_fixture)

    collect_result = plugin_fixture.process_component(oci_component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to get image digest for")


@pytest.mark.parametrize(
    argnames=["response_fixture", "cred_object_fixture", "should_error", "expected_return_value"],
    argvalues=[
        pytest.param(
            {
                "status_code": 200,
                "headers": {
                    "Docker-Content-Digest": "sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00"
                },
            },
            {"password": SecretStr("*************")},
            False,
            "sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            id="success",
        ),
        pytest.param(
            {"status_code": 403},
            {},
            True,
            "",
            id="http-error",
        ),
    ],
    indirect=["response_fixture", "cred_object_fixture"],
)
def test__get_digest(
    plugin_fixture: CollectDockerPlugin,
    response_fixture: Callable[[str], Response],
    should_error: bool,
    expected_return_value: str,
    monkeypatch: pytest.MonkeyPatch,
    cred_object_fixture: CredentialRequiredService,
):
    """Test the Docker Collector _get_digest method."""
    monkeypatch.setattr(target=Registry, name="do_request", value=response_fixture)

    registry = Registry()
    container = Container(name="test", registry="https://docker.io")

    if should_error:
        with pytest.raises(expected_exception=HopprPluginError):  # noqa: PT012
            digest_result = plugin_fixture._get_digest(registry, container, cred_object_fixture)
            assert digest_result == expected_return_value
    else:
        digest_result = plugin_fixture._get_digest(registry, container, cred_object_fixture)
        assert digest_result == expected_return_value


@pytest.mark.parametrize(
    argnames=["response_fixture", "cred_object_fixture", "expected_return_value"],
    argvalues=[
        pytest.param(
            {
                "status_code": 200,
                "content": "somestuff",
            },
            {"password": SecretStr("*************")},
            "somestuff",
            id="success",
        ),
        pytest.param(
            {"status_code": 403},
            {},
            "",
            id="failure",
        ),
    ],
    indirect=["response_fixture", "cred_object_fixture"],
)
def test__get_signature_manifest(
    plugin_fixture: CollectDockerPlugin,
    response_fixture: Callable[[str], Response],
    expected_return_value: str,
    monkeypatch: pytest.MonkeyPatch,
    cred_object_fixture: CredentialRequiredService,
):
    """Test the Docker Collector _get_signature_manifest method."""
    monkeypatch.setattr(target=Registry, name="do_request", value=response_fixture)

    registry = Registry()
    container = Container(name="test", registry="https://docker.io")
    digest = "sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00"

    digest_result = plugin_fixture._get_signature_manifest(registry, container, digest, cred_object_fixture)
    assert digest_result == expected_return_value


@pytest.mark.parametrize(
    argnames=["purl", "repo_url", "expected_return_value"],
    argvalues=[
        pytest.param(
            PackageURL(
                type="docker",
                name="ubuntu",
                namespace="",
                version="sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
                subpath=None,
            ),
            "https://docker.io",
            "index.docker.io/library/ubuntu@sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
        ),
        pytest.param(
            PackageURL(type="docker", name="nginx", namespace=None, version="1.22", subpath=None),
            "docker.io",
            "index.docker.io/library/nginx@1.22",
        ),
        pytest.param(
            PackageURL(
                type="docker",
                name="mongo",
                namespace=None,
                version="40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
                subpath=None,
            ),
            "ghcr.io",
            "ghcr.io/mongo@sha256:40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
        ),
        pytest.param(
            PackageURL(type="oci", name="cosign", namespace="chainguard/", version="latest", subpath=None),
            "ghcr.io",
            "ghcr.io/chainguard/cosign@latest",
        ),
    ],
)
def test__get_oras_container(
    plugin_fixture: CollectDockerPlugin,
    purl: PackageURL,
    repo_url: str,
    expected_return_value: str,
):
    """Test the Docker Collector _get_oras_container method."""
    digest_result = plugin_fixture._get_oras_container(purl, repo_url)
    assert str(digest_result) == expected_return_value


@pytest.mark.parametrize(
    argnames="manifest",
    argvalues=[
        pytest.param(
            """{
                "mediaType": "application/vnd.oci.image.manifest.v1+json",
                "layers": [
                    {
                        "mediaType": "application/vnd.dev.cosign.simplesigning.v1+json",
                        "digest": "sha256:0c1b84ade89e3a2495a3fc569b6e8e44dac6dc1570d76fcb4af8ec6ed750c7f5"
                    },
                    {
                        "mediaType": "application/vnd.dev.cosign.simplesigning.v1+json",
                        "digest": "sha256:0c1b84ade89e3a2495a3fc569b6e8e44dac6dc1570d76fcb4af8ec6ed750c7f5"
                    }
                ]
            }""",
            id="success",
        ),
        pytest.param(
            """{
                "mediaType": "application/vnd.oci.image.manifest.v1+json",
                "layers": []
            }""",
            id="no-sigs",
        ),
    ],
)
def test__parse_manifest(
    plugin_fixture: CollectDockerPlugin,
    manifest: str,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test the Docker Collector _get_digest method."""
    monkeypatch.setattr(target=Registry, name="download_blob", value=lambda *_, **__: "target_path")

    plugin_fixture._parse_manifest(manifest, Path("component_dir"), "test_digest")


@pytest.mark.parametrize(
    argnames=["digest", "manifest", "cred_object_fixture", "should_error"],
    argvalues=[
        pytest.param(
            "sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            '{"config":{"mediaType":"application","digest":"sha256:123456789"},"layers":[{"size":100,"digest":"sha256:987654321"}]}',
            {
                "password": SecretStr("*************"),
            },
            False,
            id="success",
        ),
        pytest.param(
            "",
            "",
            {},
            False,
            id="no digest",
        ),
        pytest.param(
            "sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            "",
            {},
            False,
            id="no signature manifest",
        ),
        pytest.param(
            "",
            "",
            {},
            True,
            id="error",
        ),
    ],
    indirect=["cred_object_fixture"],
)
def test__get_package_signatures(
    plugin_fixture: CollectDockerPlugin,
    digest: str,
    manifest: str,
    cred_object_fixture: CredentialRequiredService,
    should_error: bool,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test the Docker Collector _get_package_signatures method."""

    def mock_digest(*args):
        if should_error:
            raise HopprPluginError()

        return digest

    monkeypatch.setattr(target=plugin_fixture, name="_get_digest", value=mock_digest)
    monkeypatch.setattr(target=plugin_fixture, name="_get_signature_manifest", value=lambda *_, **__: manifest)
    monkeypatch.setattr(target=plugin_fixture, name="_parse_manifest", value=lambda *_, **__: None)

    purl = PackageURL(type="oci", name="cosign", namespace="chainguard/", version="latest", subpath=None)

    plugin_fixture._get_package_signatures("ghcr.io", purl, Path(), cred_object_fixture)


@pytest.mark.parametrize(
    argnames=["annotations_field", "index"],
    argvalues=[
        pytest.param(
            {
                "dev.cosignproject.cosign/signature": "MEQCIEENfFLp3HDZEiewJBb9XH+w5UxnN1EF15puN3Hpg4aoAiAj3LWMysPLv7ZAGR1L+FVMXRANiZK+jkh7kAuBiPTL4w==",
                "dev.sigstore.cosign/certificate": "certificate",
                "dev.sigstore.cosign/bundle": "bundle",
                "dev.sigstore.cosign/chain": "chain",
            },
            0,
        ),
        pytest.param({}, 1),
    ],
)
def test___create_signature_files(
    plugin_fixture: CollectDockerPlugin,
    annotations_field: dict[str, str],
    index: int,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test the Docker Collector _create_signature_files method."""
    monkeypatch.setattr(target=Path, name="write_text", value=lambda *_, **__: None)

    plugin_fixture._create_signature_files(annotations_field, Path("testfile"), index, "test_digest")


def test_collect_docker_fail_invalid_purl(
    plugin_fixture: CollectDockerPlugin, component: Component, monkeypatch: pytest.MonkeyPatch
):
    """Test a failing run of the Docker Collector."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    component.purl = "pkg:docker/something/else:1.2.3?repository_url=somewhere.com/something/test-component&tag=latest"

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Missing `version` from purl string: pkg:docker/something/else:1.2.3?repository_url=somewhere.com/something/test-component&tag=latest. Must be a valid tag or digest"
    )


def test__get_image(plugin_fixture: CollectDockerPlugin, component: Component):
    """Test the _get_image function."""
    component.purl = "pkg:oci/test@0.0.0-test-image?repository_url=oci://test.registry/"
    purl = hoppr.utils.get_package_url(component.purl)
    assert plugin_fixture._get_image("", purl).url == "docker://test.registry/test%3A0.0.0-test-image"
