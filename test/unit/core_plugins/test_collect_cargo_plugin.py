"""Test module for CollectCargoPlugin class."""

from __future__ import annotations

from collections.abc import Callable

import pytest

from hoppr import cdx, net
from hoppr.core_plugins.collect_cargo_plugin import CollectCargoPlugin, requests
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component, ComponentType, Hash
from hoppr.models.types import PurlType
from hoppr.result import ResultStatus


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """Test Component fixture."""
    return Component(
        name="TestComponent",
        purl="pkg:cargo/example-package@1.2.3",
        type=ComponentType.FILE,
    )


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectCargoPlugin, monkeypatch: pytest.MonkeyPatch) -> CollectCargoPlugin:
    """Override and parametrize plugin_fixture to return CollectCargoPlugin."""
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere1.com"])

    plugin_fixture.context.repositories[PurlType.CARGO] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["response_generator", "component_hashes", "component_file_hash", "expected_result"],
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"checksum": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}}',
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.SUCCESS,
            id="no_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"checksum": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}}',
                },
            ],
            [
                Hash(alg=cdx.HashAlg.MD5, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_1, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_384, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
                Hash(alg=cdx.HashAlg.SHA3_384, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
            ],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.SUCCESS,
            id="correct_hashes",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"checksum": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}}',
                },
            ],
            [Hash(alg=cdx.HashAlg.SHA_1, content="0000000000000000000000000000000000000000")],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="incorrect_sbom_hash",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"checksum": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}}',
                },
            ],
            [],
            "0000000000000000000000000000000000000000",
            ResultStatus.FAIL,
            id="incorrect_file_hash",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 500,
                    "content": b"",
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="error_downloading_checksum",
        ),
        pytest.param(
            [
                {"status_code": 200, "content": b"0" * 1024},
                {
                    "status_code": 200,
                    "content": b'{"version": {"banana": "ba na na"}}',
                },
            ],
            [],
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            ResultStatus.FAIL,
            id="metadata_missing_checksum",
        ),
    ],
    indirect=["response_generator"],
)
def test_collect_cargo(
    plugin_fixture: CollectCargoPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_generator: Callable[..., requests.Response],
    component: Component,
    component_hashes: list[Hash],
    component_file_hash: str,
    expected_result: ResultStatus,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Cargo collector successful run with a given URL."""
    component.hashes = component_hashes
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_generator)
        patch.setattr(target=net, name="get_file_hash", value=lambda alg, path: component_file_hash)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.status == expected_result


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_status_code"],
    argvalues=[
        ({"status_code": 401}, 401),
        ({"status_code": 403}, 403),
        ({"status_code": 404}, 404),
        ({"status_code": 500}, 500),
        ({"status_code": 502}, 502),
    ],
    ids=[
        "Unauthorized (401)",
        "Forbidden (403)",
        "Not Found (404)",
        "Internal Server Error (500)",
        "Bad Gateway (502)",
    ],
    indirect=["response_fixture"],
)
def test_collect_cargo_fail(
    plugin_fixture: CollectCargoPlugin,
    find_credentials_fixture: CredentialRequiredService,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    expected_status_code: int,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test cargo collector unsuccessful run with a given url."""
    with monkeypatch.context() as patch:
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == (
            f"Failed to download Cargo package pkg:cargo/example-package@1.2.3, status_code={expected_status_code}"
        )


def test_get_version(plugin_fixture: CollectCargoPlugin):
    """Test cargo collector has version."""
    assert len(plugin_fixture.get_version()) > 0
