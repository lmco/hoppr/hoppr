"""Test module for OrasBundlePlugin class."""

from __future__ import annotations

from collections.abc import Callable
from pathlib import Path

import oras.auth.utils
import oras.utils
import pytest
import requests

from oras.auth.basic import BasicAuth
from oras.auth.utils import authHeader
from oras.container import Container
from requests.sessions import Session

from hoppr.core_plugins.oras_bundle import OrasBundlePlugin
from hoppr.core_plugins.oras_registry import Registry


def test_oras_registry_init():
    """Test oras registry init method."""
    reg = Registry("https://test.com", True, False, "basic")
    assert reg.hostname == "https://test.com"
    assert reg.prefix == "http"


def test_oras_push_container(plugin_fixture: OrasBundlePlugin, monkeypatch: pytest.MonkeyPatch):
    """Test the oras_registry Registry.push_container() function."""
    successful_response = requests.Response()
    successful_response.status_code = 200
    monkeypatch.setattr(target=Registry, name="upload_blobs", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Registry, name="upload_blob", value=lambda *_, **__: successful_response)
    monkeypatch.setattr(target=Registry, name="upload_manifest", value=lambda *_, **__: successful_response)

    reg = Registry("Testhost")
    container = Container("Container", "Testhost")
    result = reg.push_container(container, [container], plugin_fixture.get_logger())
    assert result.status_code == 200


@pytest.mark.parametrize(
    argnames=["response_generator", "auth_required"],
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": "body"},
            ],
            False,
            id="no-authentication-required",
        ),
        pytest.param(
            [
                {"status_code": 401, "content": "body"},
                {"status_code": 200, "content": "body"},
            ],
            True,
            id="authentication-required",
        ),
    ],
    indirect=["response_generator"],
)
def test_oras_do_request(
    monkeypatch: pytest.MonkeyPatch, response_generator: Callable[..., requests.Response], auth_required: bool
):
    """Test the oras_registry Registry.do_request() function."""
    monkeypatch.setattr(target=Registry, name="upload_blobs", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Session, name="request", value=response_generator)

    if auth_required:
        monkeypatch.setattr(target=Registry, name="authenticate_request", value=lambda *_, **__: True)

    reg = Registry("Testhost")
    result = reg.do_request(url="https://test.com/uploads")
    assert result.status_code == 200


@pytest.mark.parametrize(
    argnames=["response_generator", "response_params", "expected"],
    argvalues=[
        pytest.param(
            [{"status_code": 200, "content": b'{"token": "token"}'}],
            ["Www-Authenticate", "Www-Authenticate", None],
            True,
            id="successful-authentication",
        ),
        pytest.param(
            [{"status_code": 400, "content": b'{"token": "token"}'}],
            ["Www-Authenticate", "Www-Authenticate", None],
            False,
            id="unsuccessful-authentication",
        ),
        pytest.param(
            [{"status_code": 200, "content": b'{"token": "token"}'}],
            ["Www-Authenticate", None, None],
            False,
            id="www-header-missing",
        ),
        pytest.param(
            [{"status_code": 200, "content": b'{"token": "token"}'}],
            ["Www-Authenticate", "Www-Authenticate", "token"],
            True,
            id="token-found",
        ),
    ],
    indirect=["response_generator"],
)
def test_oras_authenticate_request(
    plugin_fixture: OrasBundlePlugin,
    monkeypatch: pytest.MonkeyPatch,
    response_params: list[str],
    response_generator: Callable[..., requests.Response],
    expected: bool,
):
    """Test the oras_registry Registry.authenticate_request() function."""
    auth_header = authHeader({"service": "Service", "realm": "test.com", "scope": "Scope"})

    monkeypatch.setattr(
        target=oras.auth.utils,
        name="parse_auth_header",
        value=lambda *_, **__: auth_header,
    )
    monkeypatch.setattr(
        target=requests.Session,
        name="get",
        value=response_generator,
    )
    reg = Registry("test.com", auth_backend="basic", logger=plugin_fixture.get_logger())
    reg.prefix = "https"
    response = requests.Response()
    response.headers[response_params[0]] = response_params[1]
    reg.token = response_params[2]

    if reg.token:
        reg.auth._basic_auth = "basic_auth"

    assert reg.authenticate_request(response) is expected


@pytest.mark.parametrize(
    argnames="response_generator",
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"body"},
                {"status_code": 200, "content": b"body"},
            ],
            id="successful-upload",
        ),
    ],
    indirect=["response_generator"],
)
def test_oras_put_upload(
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
    tmp_path: Path,
):
    """Test the oras_registry Registry.put_upload() function."""
    container = Container("TestContainer")
    container.registry = "test.registry"
    container.namespace = "test"
    container.repository = "repo"

    monkeypatch.setattr(
        target=oras.utils,
        name="append_url_params",
        value=lambda *_, **__: "test.url.com",
    )
    monkeypatch.setattr(
        target=Registry,
        name="do_request",
        value=response_generator,
    )
    monkeypatch.setattr(
        target=Registry,
        name="_get_location",
        value=lambda *_, **__: "test_session",
    )
    reg = Registry("test.com")
    reg.prefix = "https"

    Path(tmp_path / "test.txt").write_text(data="Hello world!", encoding="utf-8")
    result = reg.put_upload(str(tmp_path / "test.txt"), container, {"size": 1, "digest": "test"}, False)
    assert result.status_code == 200


@pytest.mark.parametrize(
    argnames="response_generator",
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"body"},
            ],
            id="unsuccessful-upload",
        ),
    ],
    indirect=["response_generator"],
)
def test_oras_put_upload_failure(
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
    tmp_path: Path,
):
    """Test the oras_registry Registry.authenticate_request() function with a failed session url."""
    container = Container("TestContainer")
    container.registry = "test.registry"
    container.namespace = "test"
    container.repository = "repo"

    monkeypatch.setattr(
        target=Registry,
        name="do_request",
        value=response_generator,
    )
    monkeypatch.setattr(
        target=Registry,
        name="_get_location",
        value=lambda *_, **__: None,
    )
    reg = Registry("test.com")
    reg.prefix = "https"

    with pytest.raises(expected_exception=ValueError, match=r"Expecting value: line 1 column 1 \(char 0\)"):
        reg.put_upload(str(tmp_path / "test.txt"), container, {"size": 1, "digest": "test"}, False)


@pytest.mark.parametrize(
    argnames=["response_generator", "expected", "chunked"],
    argvalues=[
        pytest.param(
            [
                {"status_code": 200, "content": b"body"},
            ],
            200,
            False,
            id="successful-upload",
        ),
        pytest.param(
            [
                {"status_code": 300, "content": b"body"},
            ],
            200,
            True,
            id="successful-chunked-upload",
        ),
    ],
    indirect=["response_generator"],
)
def test_oras_upload_blob(
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
    expected: int,
    chunked: bool,
    tmp_path: Path,
):
    """Test the oras_registry Registry.upload_blob() function."""
    container = Container("TestContainer")
    container.registry = "test.registry"
    container.namespace = "test"
    container.repository = "repo"

    monkeypatch.setattr(
        target=BasicAuth, name="get_auth_header", value=lambda *_, **__: {"Authorization": "Basic testauthorization"}
    )
    monkeypatch.setattr(target=Registry, name="do_request", value=response_generator)
    monkeypatch.setattr(target=Registry, name="put_upload", value=response_generator)
    monkeypatch.setattr(target=Registry, name="chunked_upload", value=response_generator)
    reg = Registry("test.com", auth_backend="basic")
    reg.prefix = "https"

    Path(tmp_path / "test.txt").write_text(data="Hello world!", encoding="utf-8")
    result = reg.upload_blob(
        str(tmp_path / "test.txt"),
        container,
        {
            "size": 1,
            "layer": "test",
            "digest": "sha256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
        },
        chunked,
    )
    assert result.status_code == expected
