"""Test module for CollectGemPlugin class."""

from __future__ import annotations

from collections.abc import Callable
from pathlib import Path

import pytest

from hoppr import Hash, cdx, net
from hoppr.core_plugins.collect_gem_plugin import CollectGemPlugin, requests
from hoppr.exceptions import HopprPluginError
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType

test_component = Component(
    name="TestComponent",
    purl="pkg:gem/example-package@1.2.3?platform=test",
    type="file",  # type: ignore[arg-type]
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """Test Component fixture."""
    return test_component


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectGemPlugin, monkeypatch: pytest.MonkeyPatch) -> CollectGemPlugin:
    """Override and parametrize plugin_fixture to return CollectGemPlugin."""
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_repos",
        value=lambda comp: ["https://somewhere1.com"],
    )

    plugin_fixture.context.repositories[PurlType.GEM] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"platform" : "test", "gem_uri": "elsewhere.com", \
                "sha": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_success(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector successful run with a given URL."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        patch.setattr(
            target=component,
            name="hashes",
            value=[Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
        )

        patch.setattr(target=net, name="get_file_hash", value=lambda x, y: "a0a32b159feca49e7b13b9a49ae0127ade587f8b")

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"platform" : "test", "wrong_uri": "elsewhere.com", \
                "sha": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_download_url_failure(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector successful run with a given URL."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        patch.setattr(
            target=component,
            name="hashes",
            value=[Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
        )

        patch.setattr(target=net, name="get_file_hash", value=lambda x, y: "a0a32b159feca49e7b13b9a49ae0127ade587f8b")

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"platform" : "test", "gem_uri": "elsewhere.com", \
                "sha": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_hash_validation_failure(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector successful run with a given URL."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        patch.setattr(
            target=component,
            name="hashes",
            value=[Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8c")],
        )

        patch.setattr(target=net, name="get_file_hash", value=lambda x, y: "a0a32b159feca49e7b13b9a49ae0127ade587f8b")
        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}."


@pytest.mark.parametrize(
    argnames=["response_fixture", "download_url", "package_out_path", "expected_exception"],
    argvalues=[
        pytest.param(
            {
                "status_code": 400,
                "content": b'{"platform" : "test", "gem_uri": "elsewhere.com", \
                "sha": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"}',
            },
            "some_download_url.com",
            Path("path/to/file"),
            "Failed to download Gem package pkg:gem/example-package@1.2.3?platform=test, status_code=400",
            id="failure",
        )
    ],
    indirect=["response_fixture"],
)
def test__download_gem_file(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    download_url: str,
    package_out_path: Path,
    expected_exception: str,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test _download_gem_file method."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        patch.setattr(
            target=component,
            name="hashes",
            value=[Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
        )

        patch.setattr(target=net, name="get_file_hash", value=lambda x, y: "a0a32b159feca49e7b13b9a49ae0127ade587f8b")

        try:
            plugin_fixture._download_gem_file(download_url, package_out_path, component)
        except HopprPluginError as ex:
            assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 404,
            "content": b'{"platform" : "test", "gem_uri": "elsewhere.com"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_fail_bad_purl(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector fail run if purl is inaccesible."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected Fail result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 200, "content": b'{"platform" : "test"}'}],
    indirect=True,
)
def test_collect_gem_fail_no_gem_uri(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector fail run when no gem URI has been returned."""
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected Fail result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_generator",
    argvalues=[["response_200", "response_404"]],
    indirect=True,
)
def test_collect_gem_fail_bad_gem_uri(
    plugin_fixture: CollectGemPlugin,
    response_generator: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test Gem collector fail run with a bad gem URI from the API."""
    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(
        target=requests.Response, name="json", value=lambda *_, **__: {"gem_uri": "https://gem_uri.io/link"}
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}."


@pytest.mark.parametrize(
    argnames=["package_path", "mock_generated_hash", "expected_exception"],
    argvalues=[
        pytest.param(Path("some/path/to/file"), "a0a32b159feca49e7b13b9a49ae0127ade587f8b", None, id="success"),
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8c",
            "Hash for TestComponent does not match expected hash.",
            id="failure",
        ),
    ],
)
def test__update_hashes(
    package_path: Path,
    mock_generated_hash: str,
    expected_exception: str | None,
    plugin_fixture: CollectGemPlugin,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test _update_hashes method of Gem Collector."""
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[Hash(alg=cdx.HashAlg.SHA_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
    )
    monkeypatch.setattr(target=net, name="get_file_hash", value=lambda x, y: mock_generated_hash)

    try:
        plugin_fixture._update_hashes(component, package_path)
    except (HopprPluginError, ValueError) as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["file_checksum", "expected_hash", "expected_exception"],
    argvalues=[
        pytest.param(
            Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
            Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
            None,
            id="success",
        ),
        pytest.param(
            Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8c"),
            Hash(alg=cdx.HashAlg.SHA_256, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b"),
            "Hash validation failed for TestComponent.",
            id="fail",
        ),
    ],
)
def test__validate_hashes(
    file_checksum: Hash,
    expected_hash: str,
    expected_exception: str | None,
    plugin_fixture: CollectGemPlugin,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectAptPlugin._validate_hashes method."""
    monkeypatch.setattr(target=component, name="hashes", value=[expected_hash])
    try:
        plugin_fixture._validate_hashes(component, file_checksum)
        assert file_checksum == expected_hash
    except HopprPluginError as ex:
        assert str(ex) == expected_exception  # noqa: PT017
