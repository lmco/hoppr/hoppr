"""Test module for CollectHelmPlugin class."""

from __future__ import annotations

import re

from collections.abc import Callable
from pathlib import Path
from subprocess import CompletedProcess

import pytest
import requests

import hoppr.plugin_utils
import hoppr.utils

from hoppr import Hash, cdx, net
from hoppr.core_plugins.collect_helm_plugin import CollectHelmPlugin
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component, Sbom
from hoppr.models.types import PurlType
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture(request: pytest.FixtureRequest):
    """Test Component fixture."""
    purl = request.param if hasattr(request, "param") else "pkg:helm/something/else@1.2.3"
    return Component(name="TestHelmComponent", purl=purl, type="file")  # type: ignore[arg-type]


@pytest.fixture
def load_file_fixture(request: pytest.FixtureRequest) -> Callable:
    """Test yaml.safe_load fixture."""

    def _load_file(input_file_path: Path) -> list | dict:
        data: list[dict[str, str]] = [
            {"url": "https://charts.hoppr.com/hoppr", "name": "hoppr"},
            {"url": "https://charts.hoppr.com/stable", "name": "stable"},
        ]

        return data if request.param == "raise" else {"repositories": data}

    return _load_file


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectHelmPlugin, monkeypatch: pytest.MonkeyPatch) -> CollectHelmPlugin:
    """Override and parametrize plugin_fixture to return CollectHelmPlugin."""
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=Result.success,
    )
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_repos",
        value=lambda comp: ["https://somewhere.com"],
    )

    plugin_fixture.context.repositories[PurlType.HELM] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.fixture
def sbom_fixture(resources_dir: Path) -> Sbom:
    """Test Sbom fixture."""
    sbom_path = resources_dir / "bom" / "int_helm_bom.json"
    return Sbom.parse_file(sbom_path)


@pytest.mark.parametrize(
    argnames=[
        "completed_process_fixture",
        "response_generator",
        "mock_file_hash",
        "expected_result",
    ],
    argvalues=[
        (
            {"returncode": 0},
            [
                {
                    "status_code": 200,
                    "content": b'{"urls": [{"digests": {"blake2b_256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"md5": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"sha256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"},"filename": "TEST.whl"},{"digests": '
                    b'{"blake2b_256": "9dbe10918a2eac4ae9f02f6cfe6414b7a155ccd8f7f9d4380d62fd5b955065c3",'
                    b'"md5": "941e175c276cd7d39d098092c56679a4",'
                    b'"sha256": "942c5a758f98d790eaed1a29cb6eefc7ffb0d1cf7af05c3d2791656dbd6ad1e1"},'
                    b'"filename": "TEST.tar.gz"}]}',
                },
            ],
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            Result.success(),
        ),
        (
            {"returncode": 1, "stderr": "b'404 Not Found\n'"},
            [
                {
                    "status_code": 200,
                    "content": b'{"urls": [{"digests": {"blake2b_256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"md5": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"sha256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"},"filename": "TEST.whl"},{"digests": '
                    b'{"blake2b_256": "9dbe10918a2eac4ae9f02f6cfe6414b7a155ccd8f7f9d4380d62fd5b955065c3",'
                    b'"md5": "941e175c276cd7d39d098092c56679a4",'
                    b'"sha256": "942c5a758f98d790eaed1a29cb6eefc7ffb0d1cf7af05c3d2791656dbd6ad1e1"},'
                    b'"filename": "TEST.tar.gz"}]}',
                },
            ],
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            Result.fail(
                "Failed to download else version 1.2.3 helm chart from https://somewhere.com. Chart not found."
            ),
        ),
        (
            {"returncode": 1, "stderr": "b'Some other error\n'"},
            [
                {
                    "status_code": 200,
                    "content": b'{"urls": [{"digests": {"blake2b_256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"md5": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"sha256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"},"filename": "TEST.whl"},{"digests": '
                    b'{"blake2b_256": "9dbe10918a2eac4ae9f02f6cfe6414b7a155ccd8f7f9d4380d62fd5b955065c3",'
                    b'"md5": "941e175c276cd7d39d098092c56679a4",'
                    b'"sha256": "942c5a758f98d790eaed1a29cb6eefc7ffb0d1cf7af05c3d2791656dbd6ad1e1"},'
                    b'"filename": "TEST.tar.gz"}]}',
                },
            ],
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            Result.fail("Failure after 3 attempts, final message Failed to download else version 1.2.3 helm chart"),
        ),
        (
            {"returncode": 0},
            [
                {
                    "status_code": 200,
                    "content": b'{"urls": [{"digests": {"blake2b_256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"md5": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                    b'"sha256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"},"filename": "TEST.whl"},{"digests": '
                    b'{"blake2b_256": "9dbe10918a2eac4ae9f02f6cfe6414b7a155ccd8f7f9d4380d62fd5b955065c3",'
                    b'"md5": "941e175c276cd7d39d098092c56679a4",'
                    b'"sha256": "942c5a758f98d790eaed1a29cb6eefc7ffb0d1cf7af05c3d2791656dbd6ad1e1"},'
                    b'"filename": "TEST.tar.gz"}]}',
                },
            ],
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa465",
            Result.fail("Hash for TestHelmComponent does not match expected hash."),
        ),
    ],
    ids=["command_success", "command_fail", "command_retry", "command_hash_mismatch"],
    indirect=["completed_process_fixture", "response_generator"],
)
def test_collect_helm(
    plugin_fixture: CollectHelmPlugin,
    run_command_fixture: Callable[..., CompletedProcess],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    response_generator: Callable[..., requests.Response],
    mock_file_hash: str,
    expected_result: Result,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectHelmPlugin.collect method."""
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[
            Hash(
                alg=cdx.HashAlg.SHA_256,
                content="fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            )
        ],
    )
    monkeypatch.setattr(target=requests, name="get", value=response_generator)

    monkeypatch.setattr(
        target=net,
        name="get_file_hash",
        value=lambda *_, **__: mock_file_hash,
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.status == expected_result.status
    assert collect_result.message.startswith(expected_result.message)


@pytest.mark.parametrize(
    argnames=["context_fixture", "load_file_fixture"],
    argvalues=[({"strict_repos": False}, "load"), ({"strict_repos": False}, "raise")],
    indirect=True,
)
def test_helm_no_strict(
    context_fixture: HopprContext,
    load_file_fixture: list[dict[str, str]],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test plugin creation with --no-strict flag."""
    monkeypatch.setattr(target=Path, name="exists", value=lambda self: True)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=load_file_fixture)

    plugin = CollectHelmPlugin(context=context_fixture, config={"helm_command": "helm"})
    assert plugin.system_repositories == [
        "https://charts.hoppr.com/hoppr",
        "https://charts.hoppr.com/stable",
    ]


@pytest.mark.parametrize(
    argnames=["package_path", "mock_generated_hash", "should_fail"],
    argvalues=[
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            False,
            id="success",
        ),
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8c",
            True,
            id="failure",
        ),
    ],
)
def test_update_hashes(
    plugin_fixture: CollectHelmPlugin,
    package_path: Path,
    mock_generated_hash: str,
    should_fail: bool,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectHelmPlugin._update_hashes method."""
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[
            Hash(
                alg=cdx.HashAlg.SHA_256,
                content="a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            )
        ],
    )
    monkeypatch.setattr(target=net, name="get_file_hash", value=lambda x, y: mock_generated_hash)
    if should_fail:
        with pytest.raises(HopprPluginError):
            plugin_fixture._update_hashes(component, package_path)
    else:
        plugin_fixture._update_hashes(component, package_path)
        for comp_hash in component.hashes:
            assert comp_hash.content == mock_generated_hash


def test_get_provenance_sha(
    plugin_fixture: CollectHelmPlugin,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectHelmPlugin._get_provenance_sha method."""
    sample_provenance_data = """
        FILE INFO
        files:
            some_random_package@version: sha256:fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464
        GPG signature
    """

    monkeypatch.setattr(target=Path, name="read_text", value=lambda *_, **__: sample_provenance_data)
    target_dir = Path("some/path/to/file")
    provenance_sha = plugin_fixture._get_provenance_sha(target_dir)
    assert provenance_sha == "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464"
    # fail test
    monkeypatch.setattr(target=re, name="search", value=lambda *_, **__: None)

    with pytest.raises(HopprPluginError):
        plugin_fixture._get_provenance_sha(target_dir)


@pytest.mark.parametrize(
    argnames=["mock_generated_hash", "should_fail"],
    argvalues=[
        pytest.param(
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            False,
            id="success",
        ),
        pytest.param(
            "fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa465",
            True,
            id="failure",
        ),
    ],
)
def test_verify_provenance_sha(
    plugin_fixture: CollectHelmPlugin,
    component: Component,
    mock_generated_hash: str,
    should_fail: bool,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectHelmPlugin._get_provenance_sha method."""
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[
            Hash(
                alg=cdx.HashAlg.SHA_256,
                content="fb7a99fc3d29ccb387fcf741a304124ae0c665a1d3a238024b4a54593f1fa464",
            )
        ],
    )
    if should_fail:
        with pytest.raises(HopprPluginError):
            plugin_fixture._verify_provenance_sha(component, mock_generated_hash)
    else:
        plugin_fixture._verify_provenance_sha(component, mock_generated_hash)
