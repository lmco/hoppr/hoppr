"""Test module for CollectPypiPlugin class."""

from __future__ import annotations

import importlib.util
import textwrap

from collections.abc import Callable, Iterator
from pathlib import Path
from subprocess import CompletedProcess

import pydantic.error_wrappers
import pytest
import requests

from hoppr_cyclonedx_models.cyclonedx_1_5 import Scope

import hoppr.net
import hoppr.plugin_utils

from hoppr import cdx
from hoppr.core_plugins.collect_pypi_plugin import CollectPypiPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component, ComponentType, Hash
from hoppr.models.types import PurlType
from hoppr.result import Result


def _xml_response(binary_hash: str = "", source_hash: str = "") -> bytes:
    binary_hash = binary_hash or "d897bc66115187cf797ffdba5eda3d63c83e96955f0112bf4aec830e0ddcba5e"
    source_hash = source_hash or "ea1e47e4961107a286be53a6ac3c82a9b650cede7b7a037eeded13a18fdee7bc"

    dummy_url = "https://files.example.org/packages"
    pkg_name = "TestComponent-1.2.3"

    return textwrap.dedent(
        f"""
        <!DOCTYPE html>
        <html>
        <head>
          <meta name="pypi:repository-version" content="1.1">
        </head>
        <body>
          <a href="{dummy_url}/{pkg_name}.tar.gz#sha256={source_hash}">{pkg_name}.tar.gz</a><br />
          <a href="{dummy_url}/{pkg_name}-py3-none-any.whl#sha256={binary_hash}">{pkg_name}-py3-none-any.whl</a><br />
        </body>
        </html>
        <!--SERIAL 22377434-->
        """.strip("\n")
    ).encode(encoding="utf-8")


@pytest.fixture(name="component")
def component_fixture(request: pytest.FixtureRequest) -> Component:
    """Test Component fixture."""
    hashes = getattr(request, "param", ["d897bc66115187cf797ffdba5eda3d63c83e96955f0112bf4aec830e0ddcba5e"])

    return Component(
        name="TestComponent",
        purl="pkg:pypi/TestComponent@1.2.3",
        version="1.2.3",
        type=ComponentType.FILE,
        hashes=[Hash(alg=cdx.HashAlg.SHA_256, content=hash_value) for hash_value in hashes],
    )


@pytest.fixture(name="excluded_component")
def excluded_component_fixture() -> Component:
    """Test Component with Scope Excluded fixture."""
    return Component(
        name="TestExcludedComponent",
        purl="pkg:pypi/something/not-needed@4.5.6",
        type=ComponentType.FILE,
        scope=Scope.EXCLUDED,
    )


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectPypiPlugin, monkeypatch: pytest.MonkeyPatch, tmp_path: Path
) -> CollectPypiPlugin:
    """Override and parametrize plugin_fixture to return CollectPypiPlugin."""
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=Result.success,
    )
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_repos",
        value=lambda comp: ["https://pypi.hoppr.com/hoppr/pypi"],
    )

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.context.repositories[PurlType.PYPI] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "response_generator", "use_creds"],
    argvalues=[
        pytest.param(
            {"type": "binary-only"},
            [
                {"status_code": 200, "content": _xml_response()},
            ],
            True,
            id="binary-only with credentials",
        ),
        pytest.param(
            {"type": "binary-preferred"},
            [
                {"status_code": 200, "content": _xml_response()},
            ],
            False,
            id="binary-preferred",
        ),
        pytest.param(
            {"type": "source-only"},
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                },
            ],
            False,
            id="source-only",
        ),
        pytest.param(
            {"type": "SOURCE-preferred"},
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                },
            ],
            False,
            id="SOURCE-preferred",
        ),
        pytest.param(
            {"type": "both-preferred"},
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                },
            ],
            False,
            id="both-preferred",
        ),
        pytest.param(
            {"type": "both-required"},
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                },
            ],
            False,
            id="both-required",
        ),
    ],
    indirect=["response_generator"],
)
def test_collect_pypi_success(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    response_generator: Callable[..., requests.Response],
    use_creds: bool,
    find_credentials_fixture: Callable[[str], CredentialRequiredService],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test collect method: success."""

    def get_file_paths_patch(target_dir: Path) -> list[Path]:
        return [target_dir / "TestComponent-1.2.3-py3-none-any.whl"]

    def get_artifact_hashes_patch(path: Path) -> Iterator[Hash]:
        yield from (
            Hash(alg=cdx_alg, content="d897bc66115187cf797ffdba5eda3d63c83e96955f0112bf4aec830e0ddcba5e")
            for cdx_alg in hoppr.net.HASH_ALG_MAP
        )

    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_file_paths", value=get_file_paths_patch)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_artifact_hashes",
        value=get_artifact_hashes_patch,
    )
    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(
        target=Credentials,
        name="find",
        value=find_credentials_fixture if use_creds else lambda _: None,  # type: ignore[return-value]
    )
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )

    assert plugin_fixture.config
    collection_type = plugin_fixture.config["type"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result for {collection_type}, got {collect_result}"


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_generator", "expected_message", "response_generator"],
    argvalues=[
        pytest.param(
            {"type": "both-preferred"},
            ["return_1", "return_0"],
            "Only able to download source",
            [
                {"status_code": 200, "content": _xml_response()},
                {"status_code": 400, "content": _xml_response()},
            ],
            id="[Failure, Success]",
        ),
        pytest.param(
            {"type": "both-preferred"},
            ["return_0", "return_1"],
            "Only able to download binary",
            [
                {"status_code": 400, "content": _xml_response()},
                {"status_code": 200, "content": _xml_response()},
            ],
            id="[Success, Failure]",
        ),
    ],
    indirect=["completed_process_generator", "response_generator"],
)
def test_collect_pypi_success_both_preferred(
    plugin_fixture: CollectPypiPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    expected_message: str,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
):
    """Test collect method: success on second attempt."""
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=completed_process_generator,
    )
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )
    monkeypatch.setattr(target=requests, name="get", value=response_generator)

    assert plugin_fixture.config
    collection_type = plugin_fixture.config["type"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result for {collection_type}, got {collect_result}"
    assert collect_result.message == expected_message


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_generator", "response_generator"],
    argvalues=[
        pytest.param(
            {"type": "binary-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
            ],
            [
                {"status_code": 400, "content": _xml_response()},
                {"status_code": 200, "content": _xml_response()},
            ],
            id="binary-preferred [Failure, Success]",
        ),
        pytest.param(
            {"type": "SOURCE-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
            ],
            [
                {"status_code": 400, "content": _xml_response()},
                {"status_code": 200, "content": _xml_response()},
            ],
            id="SOURCE-preferred [Failure, Success]",
        ),
        pytest.param(
            {"type": "both-preferred"},
            [
                {"args": ["git"], "returncode": 0},
                {"args": ["git"], "returncode": 1},
            ],
            [
                {"status_code": 400, "content": _xml_response()},
                {"status_code": 200, "content": _xml_response()},
            ],
            id="both-preferred [Success, Failure]",
        ),
        pytest.param(
            {"type": "both-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
            ],
            [
                {"status_code": 400, "content": _xml_response()},
                {"status_code": 200, "content": _xml_response()},
            ],
            id="both-preferred [Failure, Success]",
        ),
    ],
    indirect=["completed_process_generator", "response_generator"],
)
def test_collect_pypi_success_2nd_try(
    plugin_fixture: CollectPypiPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
):
    """Test collect method: success on second attempt."""
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=completed_process_generator,
    )
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )
    monkeypatch.setattr(target=requests, name="get", value=response_generator)

    assert plugin_fixture.config
    collection_type = plugin_fixture.config["type"]

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result for {collection_type}, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["pip_command", "response_generator"],
    argvalues=[
        pytest.param("python -m pip", [{"status_code": 200, "content": _xml_response()}], id="str"),
        pytest.param(["python", "-m", "pip"], [{"status_code": 200, "content": _xml_response()}], id="list[str]"),
    ],
    indirect=["response_generator"],
)
def test_collect_pypi_success_different_pip(
    pip_command: list[str] | str,
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
    response_generator: Callable[..., requests.Response],
):
    """Test collect method: success."""
    config = {"pip_command": pip_command}
    CollectPypiPlugin(context=plugin_fixture.context, config=config)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "Test-1.2.3.tar.gz",
            plugin_fixture.context.collect_root_dir / "Test-1.2.3.whl",
        ]),
    )
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames="config",
    argvalues=[
        pytest.param({"type": "garbage"}),
    ],
    indirect=[],
)
def test_collect_pypi_config_validation_error(
    context_fixture: HopprContext,
    config: dict[str, str],
):
    """Test collect method: validation error."""
    with pytest.raises(pydantic.error_wrappers.ValidationError):
        CollectPypiPlugin(context_fixture, config)


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames=[
        "config_fixture",
        "completed_process_generator",
        "expected_msg",
        "component_hashes",
        "response_generator",
    ],
    argvalues=[
        pytest.param(
            {"type": "binary-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
            ],
            "Failed to download testcomponent version 1.2.3. Unable to download binary or source.",
            [],
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                }
            ],
            id="binary-preferred [Unable to download binary or source]",
        ),
        pytest.param(
            {"type": "source-only"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
            ],
            "Failed to download testcomponent version 1.2.3. Unable to collect source",
            [],
            [
                {
                    "status_code": 200,
                    "content": _xml_response(),
                }
            ],
            id="source-only [Unable to collect source]",
        ),
        pytest.param(
            {"type": "SOURCE-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
            ],
            "Failed to download testcomponent version 1.2.3. Unable to download source or binary.",
            [],
            [
                {
                    "status_code": 200,
                    "content": b"""<!DOCTYPE html><html>  <head>
                    <meta name="pypi:repository-version" content="1.1">
                            </head>  <body>    </body></html><!--SERIAL 22377434-->""",
                }
            ],
            id="SOURCE-preferred [Unable to download source or binary]",
        ),
        pytest.param(
            {"type": "both-preferred"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
            ],
            "Failed to download testcomponent version 1.2.3. Unable to download binary or source.",
            [],
            [
                {
                    "status_code": 200,
                    "content": b"""<!DOCTYPE html><html>  <head>
                            <meta name="pypi:repository-version" content="1.1">
                                  </head>  <body>    </body></html><!--SERIAL 22377434-->""",
                }
            ],
            id="both-preferred [Unable to download binary or source]",
        ),
        pytest.param(
            {"type": "both-required"},
            [
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
            ],
            "Failed to download testcomponent version 1.2.3. Unable to download binary or source.",
            [],
            [
                {
                    "status_code": 200,
                    "content": b"""<!DOCTYPE html><html>  <head>
                            <meta name="pypi:repository-version" content="1.1">
                                    </head>  <body>
                                            </body></html><!--SERIAL 22377434-->""",
                }
            ],
            id="both-required [Unable to download binary or source]",
        ),
        pytest.param(
            {"type": "both-required"},
            [
                {"args": ["git"], "returncode": 0},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
                {"args": ["git"], "returncode": 1},
                {"args": ["git"], "returncode": 0},
                {"args": ["git"], "returncode": 1},
            ],
            "Failed to download testcomponent version 1.2.3. Only able to download binary",
            [],
            [
                {
                    "status_code": 200,
                    "content": b"""<!DOCTYPE html>
<html>
  <head>
    <meta name="pypi:repository-version" content="1.1">
  </head>
  <body>
<a
href="https://files.pythonhosted.org/packages/51/41/9d109e02a271a14131a5f6fd4508ff8d6d369b8ee0eaaa4184499eea1e9a/TEST.tar.gz#sha256=a0a32b159feca49e7b13b9a49ae0127ade587f8b">TEST-1.2.3.tar.gz</a><br />
<a
href="https://files.pythonhosted.org/packages/60/7e/ca734b40ef59d7aac6d13900bb50d8c99b3b52a79786dea6f1191802badf/TEST-1.2.3.whl#sha256=a0a32b159feca49e7b13b9a49ae0127ade587f8b" data-dist-info-metadata="sha256=a0a32b159feca49e7b13b9a49ae0127ade587f8b"
data-core-metadata="sha256=4083a6f0ec6612424c2f628a654ba86def396eec8ef1744b4e0c53266833281d">TEST-1.2.3.whl</a><br />
</body>
</html>
<!--SERIAL 22377434-->""",
                }
            ],
            id="both-required [Only able to download binary]",
        ),
        pytest.param(
            {"type": "binary-only"},
            [{"args": ["git"], "returncode": 0}],
            "Failed to download testcomponent version 1.2.3. Hash validation failed for TestComponent.",
            [],
            [
                {
                    "status_code": 200,
                    "content": _xml_response(binary_hash="0" * 64),
                },
            ],
            id="binary-only [Hash validation failed]",
        ),
        pytest.param(
            {"type": "binary-only"},
            [{"args": ["git"], "returncode": 0}],
            "Download URL not found for filename: 'testcomponent-1.2.3-py3-none-any.whl'",
            [],
            [
                {
                    "status_code": 200,
                    "content": b"""<!DOCTYPE html><html>  <head>
                    <meta name="pypi:repository-version" content="1.1">
                    </head>  <body>  </body></html><!--SERIAL 22377434-->""",
                },
            ],
            id="binary-only [No response returned]",
        ),
        pytest.param(
            {"type": "binary-only"},
            [{"args": ["git"], "returncode": 0}],
            "Failed to download testcomponent version 1.2.3. Hash for TestComponent does not match expected hash.",
            [Hash(alg=cdx.HashAlg.SHA3_256, content="0000000000000000000000000000000000000000")],
            [{"status_code": 200, "content": _xml_response()}],
            id="binary-only [Wrong component hash]",
        ),
    ],
    indirect=["config_fixture", "completed_process_generator", "response_generator"],
)
def test_collect_pypi_fail(
    expected_msg: str,
    plugin_fixture: CollectPypiPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    component_hashes: list[Hash],
    response_generator: Callable[..., requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test collect method: fail."""
    component.hashes = component_hashes

    def get_file_paths_patch(target_dir: Path) -> list[Path]:
        return [target_dir / "TestComponent-1.2.3-py3-none-any.whl"]

    def get_artifact_hashes_patch(path: Path) -> Iterator[Hash]:
        yield from (
            Hash(alg=cdx_alg, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b") for cdx_alg in hoppr.net.HASH_ALG_MAP
        )

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=completed_process_generator,
    )
    monkeypatch.setattr(
        target=Result,
        name="retry",
        value=Result.fail,
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_file_paths", value=get_file_paths_patch)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_artifact_hashes",
        value=get_artifact_hashes_patch,
    )
    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == expected_msg


def test_collect_pypi_pip_not_found(
    plugin_fixture: CollectPypiPlugin, component: Component, monkeypatch: pytest.MonkeyPatch
):
    """Test collect method: pip not found."""
    monkeypatch.setattr(target=importlib.util, name="find_spec", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()
    assert collect_result.message == "The pip package was not found. Please install and try again."


def test_collector_excluded_pypi(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    excluded_component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test raw collector skip run given a purl with a scope of excluded."""
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(
        target=Path,
        name="iterdir",
        value=lambda *_, **__: iter([
            plugin_fixture.context.collect_root_dir / "pytest.tar.gz",
            plugin_fixture.context.collect_root_dir / "pytest.whl",
        ]),
    )
    collect_result = plugin_fixture.process_component(excluded_component)
    assert collect_result.is_excluded(), f"Expected EXCLUDED result, got {collect_result}"


@pytest.mark.parametrize(
    argnames="file_attrs",
    argvalues=[
        pytest.param(
            [
                ("cp310", "cp310", "win_amd64"),
                ("cp37", "cp37m", "win_amd64"),
                ("cp38", "cp38", "win_amd64"),
                ("cp39", "cp39", "win_amd64"),
            ],
            id="windows-amd64",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "win32"),
                ("cp37", "cp37m", "win32"),
                ("cp38", "cp38", "win32"),
                ("cp39", "cp39", "win32"),
            ],
            id="windows-win32",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "manylinux_2_17_x86_64.manylinux2014_x86_64"),
                ("cp37", "cp37m", "manylinux_2_17_x86_64.manylinux2014_x86_64"),
                ("cp38", "cp38", "manylinux_2_17_x86_64.manylinux2014_x86_64"),
                ("cp39", "cp39", "manylinux_2_17_x86_64.manylinux2014_x86_64"),
                ("cp310", "cp310", "musllinux_1_1_x86_64"),
                ("cp37", "cp37m", "musllinux_1_1_x86_64"),
                ("cp38", "cp38", "musllinux_1_1_x86_64"),
                ("cp39", "cp39", "musllinux_1_1_x86_64"),
            ],
            id="linux-x86_64",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "manylinux_2_17_aarch64.manylinux2014_aarch64"),
                ("cp310", "cp310", "musllinux_1_1_aarch64"),
                ("cp39", "cp39", "manylinux_2_17_aarch64.manylinux2014_aarch64"),
                ("cp39", "cp39", "musllinux_1_1_aarch64"),
                ("cp38", "cp38", "manylinux_2_17_aarch64.manylinux2014_aarch64"),
                ("cp38", "cp38", "musllinux_1_1_aarch64"),
                ("cp37", "cp37m", "manylinux_2_17_aarch64.manylinux2014_aarch64"),
                ("cp37", "cp37m", "musllinux_1_1_aarch64"),
            ],
            id="linux-aarch64",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "musllinux_1_1_i686"),
                ("cp39", "cp39", "musllinux_1_1_i686"),
                ("cp38", "cp38", "musllinux_1_1_i686"),
                ("cp37", "cp37m", "musllinux_1_1_i686"),
            ],
            id="linux-i686",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "macosx_10_9_universal2"),
                ("cp39", "cp39", "macosx_10_9_universal2"),
                ("cp38", "cp38", "macosx_10_9_universal2"),
            ],
            id="darwin-arm",
        ),
        pytest.param(
            [
                ("cp310", "cp310", "macosx_10_9_x86_64"),
                ("cp39", "cp39", "macosx_10_9_x86_64"),
                ("cp38", "cp38", "macosx_10_9_x86_64"),
                ("cp37", "cp37m", "macosx_10_9_x86_64"),
            ],
            id="darwin-intel",
        ),
    ],
)
def test__map_download_urls(plugin_fixture: CollectPypiPlugin, file_attrs: list[tuple[str, str, str]]):
    """Test the CollectPypiPlugin._map_download_urls method."""
    pkg_links: list[dict[str, str]] = [
        {
            "@href": "https://files.example.org/packages/example-1.0.0.tar.gz",
            "#text": "example-1.0.0.tar.gz",
        }
    ]

    for params in file_attrs:
        interpreter, abi, tag = params
        pkg_links.append({
            "@href": f"https://files.example.org/packages/example-1.0.0-{interpreter}-{abi}-{tag}.whl",
            "#text": f"example-1.0.0-{interpreter}-{abi}-{tag}.whl",
        })

    expected_file_link_map: dict[str, str] = {link["#text"]: link["@href"] for link in pkg_links}

    component = Component(
        type=ComponentType.LIBRARY,
        name="example",
        version="1.0.0",
        purl="pkg:pypi/example@1.0.0",
    )

    result = plugin_fixture._map_download_urls(component, pkg_links)

    assert result == expected_file_link_map, f"Expected {expected_file_link_map}, got {result}"
