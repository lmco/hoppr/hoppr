"""Test module for CollectRawPlugin class."""

from __future__ import annotations

import shutil

from collections.abc import Callable
from pathlib import Path

import pytest
import requests

from hoppr_cyclonedx_models.cyclonedx_1_5 import Scope

import hoppr.net

from hoppr.core_plugins import collect_raw_plugin
from hoppr.core_plugins.collect_raw_plugin import CollectRawPlugin
from hoppr.models.sbom import Component


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """Test Component fixture."""
    return Component(
        name="TestComponent",
        purl="pkg:generic/something/else@1.2.3",
        type="file",  # type: ignore[arg-type]
    )


@pytest.fixture(name="excluded_component")
def excluded_component_fixture() -> Component:
    """Test Component with Scope Excluded fixture."""
    return Component(
        name="TestExcludedComponent",
        purl="pkg:generic/something/not-needed@4.5.6",
        type="file",  # type: ignore[arg-type]
        scope=Scope.excluded,
    )


def get_repos(*args, **kwargs) -> list[str]:
    """Mock _get_repos method."""
    return ["https://somewhere.com"]


def get_file_repo(*args, **kwargs) -> list[str]:
    """Mock _get_repos method but with a file scheme."""
    return ["file://somewhere.com"]


def test_collector_raw_url(
    plugin_fixture: CollectRawPlugin,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test raw collector successful run with a given url."""
    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=collect_raw_plugin, name="download_file", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 404, "reason": "mocked download fail message"}],
    indirect=True,
)
def test_collector_raw_download_file_fail(
    plugin_fixture: CollectRawPlugin,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test raw collector fail download."""
    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=collect_raw_plugin, name="download_file", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "HTTP Status Code: 404; mocked download fail message"


@pytest.mark.parametrize(
    argnames=["response_fixture", "component_file_hash"],
    argvalues=[
        pytest.param(
            {"status_code": 200, "content": b""},
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
        ),
    ],
    indirect=["response_fixture"],
)
def test_collector_raw_file(
    plugin_fixture: CollectRawPlugin,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
    component_file_hash: str,
):
    """Test raw collector successful run given a file."""
    copy_mock_output = "some_destination"

    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_file_repo)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(target=shutil, name="copy", value=lambda src, dst: copy_mock_output)
    monkeypatch.setattr(
        target=hoppr.net,
        name="get_file_hash",
        value=lambda alg, path: component_file_hash,
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 404, "content": b""}],
    indirect=True,
)
def test_collector_raw_file_not_found(
    plugin_fixture: CollectRawPlugin,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test raw collector when file cannot be found."""
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_file_repo)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: False)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"


def test_collector_excluded_raw_file(plugin_fixture: CollectRawPlugin, excluded_component: Component):
    """Test raw collector skip run given a purl with a scope of excluded."""
    collect_result = plugin_fixture.process_component(excluded_component)
    assert collect_result.is_excluded(), f"Expected EXCLUDED result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["response_fixture", "component_file_hash", "expected_result", "raises"],
    argvalues=[
        pytest.param(
            {"status_code": 200, "content": b""},
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            "Unexpected Exception: Hash for TestComponent does not match expected hash.",
            True,
            id="Hashes do not match exception",
        ),
        pytest.param(
            {"status_code": 200, "content": b""},
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            "",
            False,
            id="Hashes match",
        ),
    ],
    indirect=["response_fixture"],
)
def test__validate_hashes(
    monkeypatch: pytest.MonkeyPatch,
    plugin_fixture: CollectRawPlugin,
    component: Component,
    response_fixture: Callable[[str], requests.Response],
    component_file_hash: str,
    expected_result: str,
    raises: bool,
):
    """Test the _validate_hashes function of collect_raw_plugin."""

    def update_hashes_patch(_):
        raise ValueError("pytest ValueError")

    if raises:
        monkeypatch.setattr(target=component, name="update_hashes", value=update_hashes_patch)

    copy_mock_output = "some_destination"

    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_file_repo)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(target=shutil, name="copy", value=lambda src, dst: copy_mock_output)
    monkeypatch.setattr(
        target=hoppr.net,
        name="get_file_hash",
        value=lambda alg, path: component_file_hash,
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.message == expected_result, f"Expected {expected_result} result, got {collect_result.message}"
