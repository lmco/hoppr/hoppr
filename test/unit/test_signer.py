"""Test module for Signer class."""

from __future__ import annotations

from pathlib import Path
from typing import TypeAlias

import pytest

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import dh, ec, ed25519, rsa
from cryptography.hazmat.primitives.asymmetric.types import PrivateKeyTypes
from cryptography.hazmat.primitives.serialization.ssh import SSHPrivateKeyTypes

from hoppr.exceptions import HopprPrivateKeyError
from hoppr.signer import HopprSigner

MonkeyPatch: TypeAlias = pytest.MonkeyPatch


def test_hoppr_signer_initialization_with_valid_private_key(monkeypatch: pytest.MonkeyPatch):
    """Test initialization."""
    key = rsa.generate_private_key(65537, 2048)

    private_key_bytes = key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption(),
    )
    private_key = serialization.load_pem_private_key(
        private_key_bytes,
        password=None,
        backend=default_backend(),
    )

    with monkeypatch.context() as patch:
        patch.setattr(
            target=serialization,
            name="load_pem_private_key",
            value=lambda bytes, password, backend: private_key,
        )
        patch.setattr(target=Path, name="read_text", value=lambda _: "")

        signer = HopprSigner(sign=True, functionary_key_path=Path(), functionary_key_password=None)

        assert signer.private_key is not None
        assert isinstance(signer.private_key, rsa.RSAPrivateKey)


def test_hoppr_signer_initialization_with_invalid_private_key(monkeypatch: pytest.MonkeyPatch):
    """Test unsupported key type initialization."""
    with monkeypatch.context() as patch:
        patch.setattr(
            target=serialization,
            name="load_pem_private_key",
            value=lambda byetes, password, backend: dh.DHPrivateKey,
        )
        patch.setattr(target=Path, name="read_text", value=lambda _: "")

        with pytest.raises(HopprPrivateKeyError):
            HopprSigner(
                sign=True,
                functionary_key_path=Path("/path/to/key"),
                functionary_key_password=None,
            )


def test_hoppr_signer_sign_blob_rsa(monkeypatch: pytest.MonkeyPatch):
    """Test rsa key type."""
    key = rsa.generate_private_key(65537, 2048)

    private_key_bytes = key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption(),
    )
    private_key = serialization.load_pem_private_key(
        private_key_bytes,
        password=None,
        backend=default_backend(),
    )

    _assert_signer(monkeypatch, private_key, "load_pem_private_key", private_key_bytes)


def test_hoppr_signer_sign_blob_ecdsa(monkeypatch: pytest.MonkeyPatch):
    """Test ecdsa key type."""
    key = ec.generate_private_key(ec.SECP256R1(), default_backend())

    private_key_bytes = key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption(),
    )
    private_key = serialization.load_pem_private_key(
        private_key_bytes,
        password=None,
        backend=default_backend(),
    )

    _assert_signer(monkeypatch, private_key, "load_pem_private_key", private_key_bytes)


def test_hoppr_signer_sign_blob_ed25519(monkeypatch: pytest.MonkeyPatch):
    """Test ed25519 key type."""
    key = ed25519.Ed25519PrivateKey.generate()

    private_key_bytes = key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.OpenSSH,
        serialization.NoEncryption(),
    )
    private_key = serialization.load_ssh_private_key(
        private_key_bytes,
        password=None,
        backend=default_backend(),
    )

    _assert_signer(monkeypatch, private_key, "load_ssh_private_key", private_key_bytes)


def test_hoppr_signer_sign_blobs(monkeypatch: pytest.MonkeyPatch):
    """Test signing multiple blobs."""
    key = rsa.generate_private_key(65537, 2048)

    private_key_bytes = key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption(),
    )
    private_key = serialization.load_pem_private_key(
        private_key_bytes,
        password=None,
        backend=default_backend(),
    )

    with monkeypatch.context() as patch:
        signature_writes = []
        patch.setattr(
            target=serialization,
            name="load_pem_private_key",
            value=lambda bytes, password, backend: private_key,
        )
        patch.setattr(target=Path, name="read_text", value=lambda _: private_key_bytes.decode())
        patch.setattr(target=Path, name="read_bytes", value=lambda _: bytes("some stuff", "utf-8"))
        patch.setattr(
            target=Path,
            name="write_bytes",
            value=lambda _, __: signature_writes.append("wrote signature"),
        )

        HopprSigner(sign=True, functionary_key_path=Path(), functionary_key_password=None).sign_blobs([
            Path("one"),
            Path("two"),
            Path("three"),
        ])

        assert len(signature_writes) == 3


def _assert_signer(
    monkeypatch: pytest.MonkeyPatch,
    private_key: PrivateKeyTypes | SSHPrivateKeyTypes,
    private_key_method: str,
    private_key_bytes: bytes,
):
    with monkeypatch.context() as patch:
        signature_writes = []
        patch.setattr(
            target=serialization,
            name=private_key_method,
            value=lambda bytes, password, backend: private_key,
        )
        patch.setattr(target=Path, name="read_text", value=lambda _: private_key_bytes.decode())
        patch.setattr(target=Path, name="read_bytes", value=lambda _: bytes("some stuff", "utf-8"))
        patch.setattr(
            target=Path,
            name="write_bytes",
            value=lambda _, __: signature_writes.append("wrote signature"),
        )

        HopprSigner(sign=True, functionary_key_path=Path(), functionary_key_password=None).sign_blob(Path("one"))

        assert len(signature_writes) == 1
