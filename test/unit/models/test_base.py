"""Test module for base pydantic models."""

from __future__ import annotations

from hoppr.models.base import HopprBaseSchemaModel, HopprMetadata


def test_hoppr_base_schema_model():
    """Test the Metadata, HopprBaseModel, and HopprBaseSchemaModel classes."""
    metadata = HopprMetadata(name="Hoppr Test Metadata", version="0.0.1", description="Hoppr Test Metadata")
    model = HopprBaseSchemaModel(kind="Manifest", metadata=metadata, schemaVersion="v1")

    assert isinstance(hash(model), int)
