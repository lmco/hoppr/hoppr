"""Define modules to skip when running unit tests."""

from __future__ import annotations

import pytest

pytest.importorskip(modname="hoppr.models.__main__")
