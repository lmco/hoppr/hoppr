"""Test module for data types pydantic models."""

from __future__ import annotations

from pathlib import Path

import pydantic
import pytest

from pydantic import AnyUrl, FileUrl, HttpUrl, ValidationError

from hoppr.models.sbom import Sbom
from hoppr.models.transfer import ComponentCoverage
from hoppr.models.types import BomAccess, LocalFile, OciFile, PurlType, RepositoryUrl, UrlFile, re


@pytest.mark.parametrize(
    argnames="access_type",
    argvalues=[
        BomAccess.NO_ACCESS,
        BomAccess.COMPONENT_ACCESS,
        BomAccess.FULL_ACCESS,
    ],
)
def test_bom_access(access_type: BomAccess, resources_dir: Path):
    """Test BomAccess enum."""
    bom = Sbom.parse_file(resources_dir / "bom" / "unit_bom1_mini.json")

    assert bom.components is not None
    component = bom.components[0]

    if access_type == BomAccess.NO_ACCESS:
        assert access_type.has_access_to(None)

    if access_type == BomAccess.COMPONENT_ACCESS:
        assert access_type.has_access_to(component)

    if access_type == BomAccess.FULL_ACCESS:
        assert access_type.has_access_to(bom)


def test_component_coverage():
    """Test ComponentCoverage enum."""
    assert str(ComponentCoverage.AT_LEAST_ONCE) == "AT_LEAST_ONCE"
    assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(1) is True
    assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(0) is False

    assert str(ComponentCoverage.EXACTLY_ONCE) == "EXACTLY_ONCE"
    assert ComponentCoverage.EXACTLY_ONCE.accepts_count(1) is True
    assert ComponentCoverage.EXACTLY_ONCE.accepts_count(2) is False

    assert str(ComponentCoverage.NO_MORE_THAN_ONCE) == "NO_MORE_THAN_ONCE"
    assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(0) is True
    assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(2) is False

    assert str(ComponentCoverage.OPTIONAL) == "OPTIONAL"
    assert ComponentCoverage.OPTIONAL.accepts_count(0) is True
    assert ComponentCoverage.OPTIONAL.accepts_count(-1) is False


def test_local_file():
    """Test LocalFile.__str__ method."""
    local_file = "/local/file/test.json"

    assert str(LocalFile(local=Path(local_file))) == f"file://{local_file}"


def test_oci_file():
    """Test OciFile.__str__ method."""
    oci_url = "oci://example.com/oci/file/test:v1"

    assert str(OciFile(oci=AnyUrl(url=oci_url, scheme="oci"))) == oci_url
    assert str(OciFile(oci=oci_url)) == oci_url


def test_purl_type__missing_():
    """Test PurlType._missing_ method."""
    assert PurlType("CaRGo") == PurlType.CARGO


def test_repository_url_validator(monkeypatch: pytest.MonkeyPatch):
    """Test pydantic validator for RepositoryUrl model."""
    with pytest.raises(expected_exception=ValueError, match="Input parameter `url` must be a non-empty string"):
        RepositoryUrl(url="")

    with monkeypatch.context() as patch:
        patch.setattr(target=re, name="search", value=lambda *args, **kwargs: None)

        with pytest.raises(expected_exception=ValidationError) as pytest_exception:  # noqa: PT012
            RepositoryUrl(url="invalid URL string")
            assert "Not a valid URL: invalid URL string" in str(pytest_exception)

    url = RepositoryUrl(
        url="https://test_username:test_password@example.com:443/path/to/endpoint?q=query-param#url-fragment"
    )

    assert str(url) == "https://test_username:test_password@example.com:443/path/to/endpoint?q=query-param#url-fragment"
    assert repr(url) == (
        "RepositoryUrl("
        "url='https://test_username:test_password@example.com:443/path/to/endpoint?q=query-param#url-fragment', "
        "scheme='https', username='test_username', password=SecretStr('**********'), hostname='example.com', port=443, "
        "path='/path/to/endpoint', query='q=query-param', fragment='url-fragment', "
        "netloc='test_username:test_password@example.com:443')"
    )

    assert url.scheme == "https"
    assert url.username == "test_username"
    assert isinstance(url.password, pydantic.SecretStr)
    assert url.password.get_secret_value() == "test_password"
    assert url.hostname == "example.com"
    assert url.port == 443
    assert url.path == "/path/to/endpoint"
    assert url.query == "q=query-param"
    assert url.fragment == "url-fragment"
    assert url.netloc == "test_username:test_password@example.com:443"

    url = RepositoryUrl(url="file:")
    assert url.url == "file://"
    assert url == RepositoryUrl(url="file://")

    url = RepositoryUrl(url="docker.io")
    assert url.url == "docker.io"
    assert url == RepositoryUrl(url="docker.io")


def test_repository_url_join():
    """Test RepositoryUrl.join method."""
    url = RepositoryUrl(url="https://example.com:443")
    assert url / "/path/" / "/to/" / "/endpoint/" == RepositoryUrl(url="https://example.com:443/path/to/endpoint")

    url /= "//path//to//endpoint//"
    assert url == RepositoryUrl(url="https://example.com:443/path/to/endpoint")


def test_url_file():
    """Test UrlFile.__str__ method."""
    https_url = "https://example.com/url/file/test.json"
    file_url = "file:///local/file/test.json"

    assert str(UrlFile(url=HttpUrl(url=https_url, scheme="https"))) == https_url
    assert str(UrlFile(url=FileUrl(url=file_url, scheme="file"))) == file_url
    assert str(UrlFile(url=AnyUrl(url=https_url, scheme="https"))) == https_url
    assert str(UrlFile(url=https_url)) == https_url
