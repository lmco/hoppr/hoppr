"""Test module for CollectRpmPlugin class."""

from __future__ import annotations

import gzip
import textwrap

from collections.abc import Callable
from pathlib import Path

import pytest
import requests
import xmltodict

from packageurl import PackageURL

import hoppr
import hoppr.net

from hoppr import Hash, cdx
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import SearchSequence
from hoppr.models.sbom import Component, ComponentType, Property, Sbom
from hoppr.models.types import RepositoryUrl
from hoppr.plugins.collect.rpm import CollectRpmPlugin, RepoConfig, _file_exists
from hoppr.result import Result

TEST_REPOMD_XML = textwrap.dedent(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <repomd xmlns="http://rpm.hoppr.com/metadata/repo" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm">
      <revision>1667635478</revision>
      <data type="primary">
        <location href="repodata/test-primary.xml.gz"/>
      </data>
      <data type="filelists">
        <location href="repodata/test-filelists.xml.gz"/>
      </data>
      <data type="other">
        <location href="repodata/test-other.xml.gz"/>
      </data>
    </repomd>
    """
).strip("\n")

TEST_PRIMARY_XML = textwrap.dedent(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <metadata xmlns="http://rpm.hoppr.com/metadata/common" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm" packages="2">
    <package type="rpm">
      <name>hoppr-test-rpm</name>
      <arch>x86_64</arch>
      <version epoch="0" ver="1.2.3" rel="1.fc37"/>
      <checksum type="sha" pkgid="YES">e411ebe8d6cbaa6e1412d34e74e739d14106fac76ef027c80540d3af6544d077</checksum>
      <url>https://github.com/hoppr/test-rpm-1</url>
      <location href="Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"/>
    </package>
    <package type="rpm">
      <name>hoppr-test-rpm</name>
      <arch>x86_64</arch>
      <version epoch="1" ver="4.5.6" rel="7.fc37"/>
      <checksum type="sha256" pkgid="YES">80533ab0c74f574156ced118e6c429ba045c1697646fdee441533618e90a41f3</checksum>
      <url>https://github.com/hoppr/test-rpm-2</url>
      <location href="Packages/h/hoppr-test-rpm-4.5.6-7.fc37.x86_64.rpm"/>
    </package>
    </metadata>
    """
).strip("\n")


@pytest.fixture(name="component")
def component_fixture(pkg_url: PackageURL):
    """Fixture to return Component object from PackageURL data."""
    return Component(name=pkg_url.name, version=pkg_url.version, purl=str(pkg_url), type=ComponentType.LIBRARY)


@pytest.fixture(name="pkg_url")
def pkg_url_fixture(request: pytest.FixtureRequest) -> PackageURL:
    """Fixture to return PackageURL object."""
    param_dict = dict(getattr(request, "param", {}))

    param_dict["type"] = param_dict.get("type", "rpm")
    param_dict["namespace"] = param_dict.get("namespace", "fedora")
    param_dict["name"] = param_dict.get("name", "hoppr-test-rpm")
    param_dict["version"] = param_dict.get("version", "1.2.3-1.fc37")
    param_dict["qualifiers"] = param_dict.get("qualifiers", {"arch": "x86_64", "distro": "fedora-37", "epoch": "0"})
    param_dict["subpath"] = param_dict.get("subpath")

    return PackageURL(**param_dict)


@pytest.fixture(name="sbom")
def sbom_fixture(resources_dir: Path) -> Sbom:
    """Fixture to return an Sbom object with a repository search sequence property on each component."""
    sbom = Sbom.load(source=resources_dir / "bom" / "int_dnf_bom.json")

    search_sequence = SearchSequence(
        repositories=[
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
        ]
    )

    for component in sbom.components:
        component.properties.append(
            Property(
                name="hoppr:repository:component_search_sequence",
                value=search_sequence.json(),
            )
        )

    return sbom


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_exception", "file_exists"],
    argvalues=[
        pytest.param(
            {"content": b"0" * 1048576},
            None,
            True,
            id="download-success",
        ),
        pytest.param(
            {"status_code": 404, "reason": "not found"},
            "HTTP Status Code: 404; not found",
            False,
            id="download-fail",
        ),
        pytest.param(
            {"status_code": 500, "reason": "server error"},
            "HTTP Status Code: 500; server error",
            False,
            id="download-retry",
        ),
    ],
    indirect=["response_fixture"],
)
def test__download_component(
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    expected_exception: str | None,
    file_exists: bool,
    monkeypatch: pytest.MonkeyPatch,
    tmp_path: Path,
):
    """Test CollectRpmPlugin._download_component method."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    download_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"
    dest_file = tmp_path / "hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"

    try:
        plugin_fixture._download_component(download_url, dest_file)
    except HopprPluginError as ex:
        assert str(ex) == expected_exception  # noqa: PT017

    assert dest_file.is_file() == file_exists


@pytest.mark.parametrize(
    argnames=["pkg_url", "expected_download_info", "expected_exception"],
    argvalues=[
        pytest.param(
            {"version": "1.2.3-1.fc37"},
            (
                "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm",
                "sha256",
                "e411ebe8d6cbaa6e1412d34e74e739d14106fac76ef027c80540d3af6544d077",
            ),
            None,
            id="hoppr-test-rpm@1.2.3",
        ),
        pytest.param(
            {"version": "4.5.6-7.fc37"},
            (
                "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-4.5.6-7.fc37.x86_64.rpm",
                "sha256",
                "80533ab0c74f574156ced118e6c429ba045c1697646fdee441533618e90a41f3",
            ),
            None,
            id="hoppr-test-rpm@4.5.6",
        ),
        pytest.param(
            {"version": "1.2.3"},
            None,
            "Failed to parse version string from PURL: "
            "'pkg:rpm/fedora/hoppr-test-rpm@1.2.3?arch=x86_64&distro=fedora-37&epoch=0'",
            id="version-no-dash",
        ),
        pytest.param(
            {"name": "missing-test-rpm"},
            None,
            "RPM package not found in repository: "
            "'pkg:rpm/fedora/missing-test-rpm@1.2.3-1.fc37?arch=x86_64&distro=fedora-37&epoch=0'",
            id="package-not-found",
        ),
        pytest.param(
            {
                "qualifiers": {
                    "arch": "x86_64",
                    "distro": "fedora-37",
                    "epoch": "0",
                    "repository_url": "https://rpm.other.com/dl/8/AppStream/x86_64/os",
                }
            },
            None,
            "Purl-specified repository url (https://rpm.other.com/dl/8/AppStream/x86_64/os) "
            "does not match current repo (https://rpm.hoppr.com/dl/8/AppStream/x86_64/os).",
            id="download-url-not-in-manifest",
        ),
    ],
    indirect=["pkg_url"],
)
def test_get_download_url(
    pkg_url: PackageURL,
    expected_download_info: tuple[str, hoppr.net.HashlibAlgs, str] | None,
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._get_download_url method."""
    repo = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    download_info: tuple[str, hoppr.net.HashlibAlgs, str] | None = None

    try:
        download_info = plugin_fixture._get_download_url(purl=pkg_url, repo_url=repo)
        assert download_info == expected_download_info
    except HopprPluginError as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames="pkg_url",
    argvalues=[
        pytest.param(
            {
                "qualifiers": {
                    "arch": "x86_64",
                    "distro": "fedora-37",
                    "epoch": "0",
                    "repository_url": "https://rpm.other.com/dl/8/AppStream/x86_64/os",
                }
            },
        ),
    ],
    indirect=["pkg_url"],
)
def test_get_download_url_bad_hash_type(
    pkg_url: PackageURL,
    plugin_fixture: CollectRpmPlugin,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._get_download_url method."""
    repo = "https://rpm.other.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo,
        value=xmltodict.parse(
            xml_input=textwrap.dedent(
                """\
                <?xml version="1.0" encoding="UTF-8"?>
                <metadata xmlns="http://rpm.hoppr.com/metadata/common" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm" packages="2">
                <package type="rpm">
                  <name>hoppr-test-rpm</name>
                  <arch>x86_64</arch>
                  <version epoch="0" ver="1.2.3" rel="1.fc37"/>
                  <checksum type="badhashtype" pkgid="YES">e411ebe8d6cbaa6e1412d34e74e739d14106fac76ef027c80540d3af6544d07790er8qy</checksum>
                  <url>https://github.com/hoppr/test-rpm-1</url>
                  <location href="Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"/>
                </package>
                <package type="rpm">
                  <name>hoppr-test-rpm</name>
                  <arch>x86_64</arch>
                  <version epoch="1" ver="4.5.6" rel="7.fc37"/>
                  <checksum type="sha256" pkgid="YES">80533ab0c74f574156ced118e6c429ba045c1697646fdee441533618e90a41f3</checksum>
                  <url>https://github.com/hoppr/test-rpm-2</url>
                  <location href="Packages/h/hoppr-test-rpm-4.5.6-7.fc37.x86_64.rpm"/>
                </package>
                </metadata>
                """
            ).strip("\n"),
            force_list=["package"],
        ),
    )

    with pytest.raises(HopprPluginError):
        plugin_fixture._get_download_url(purl=pkg_url, repo_url=repo)


@pytest.mark.parametrize(
    argnames=["expected_download_info", "pkg_url", "response_generator"],
    argvalues=[
        pytest.param(
            (
                "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm",
                "sha256",
                "e411ebe8d6cbaa6e1412d34e74e739d14106fac76ef027c80540d3af6544d077",
            ),
            {
                "qualifiers": {
                    "arch": "x86_64",
                    "distro": "fedora-37",
                    "epoch": "0",
                    "repository_url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
                }
            },
            [
                {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": gzip.compress(data=TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            ],
            id="repository_url-qualifier-no-strict",
        ),
    ],
    indirect=["pkg_url", "response_generator"],
)
def test_get_download_url_no_strict(
    pkg_url: PackageURL,
    expected_download_info: tuple[str, str] | None,
    response_generator: Callable[..., requests.Response],
    plugin_fixture: CollectRpmPlugin,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._get_download_url method with `--no-strict` option."""
    repo = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(target=plugin_fixture.context, name="strict_repos", value=False)
    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    download_info = plugin_fixture._get_download_url(purl=pkg_url, repo_url=repo)

    assert download_info == expected_download_info


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": gzip.compress(TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            id="primary-xml-success",
        ),
        pytest.param(
            "Failed to get primary XML data from "
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/test-primary.xml.gz",
            {"status_code": 404},
            id="primary-xml-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test_get_primary_xml_data(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._get_primary_xml_data method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")
    repomd_dict = xmltodict.parse(xml_input=TEST_REPOMD_XML, force_list=["data"])

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        rpm_data = plugin_fixture._get_primary_xml_data(repo_url, repomd_dict)
        expected_data = xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"])
        assert rpm_data == expected_data
    except HopprPluginError as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
            id="repodata-success",
        ),
        pytest.param(
            "Failed to get repository metadata from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            {"status_code": 404},
            id="repodata-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test_get_repodata(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._get_repodata method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        repodata = plugin_fixture._get_repodata(repo_url)
        expected_data = xmltodict.parse(xml_input=TEST_REPOMD_XML, force_list=["data"])
        assert repodata == expected_data
    except HopprPluginError as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["expected_exception", "cred_object_fixture", "response_generator"],
    argvalues=[
        pytest.param(
            None,
            {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
            [
                {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": gzip.compress(data=TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            ],
            id="rpm-data-success",
        ),
        pytest.param(
            "Failed to get repository metadata from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
            [{"status_code": 404}, {"status_code": 404}, {"status_code": 404}],
            id="rpm-data-fail",
        ),
    ],
    indirect=["cred_object_fixture", "response_generator"],
)
def test_populate_rpm_data(
    expected_exception: str | None,
    find_credentials_fixture: Callable[[str], CredentialRequiredService],
    plugin_fixture: CollectRpmPlugin,
    response_generator: Callable[..., requests.Response],
    monkeypatch: pytest.MonkeyPatch,
    tmp_path: Path,
):
    """Test CollectRpmPlugin._populate_rpm_data method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")

    with monkeypatch.context() as patch:
        patch.setattr(target=CollectRpmPlugin, name="rpm_data", value={})
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_generator)

        cert_file = Path(tmp_path / "test.txt")
        (tmp_path / "test.txt").write_text("test.txt")
        plugin_fixture.repo_configs.append(
            RepoConfig(
                base_url=["https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"],
                mirror_list=None,
                enabled=True,
                gpg_check=True,
                gpg_key=None,
                name=None,
                priority=1,
                proxy="_none_",
                username="mock_user",
                password="mock_password",
                ssl_ca_cert=cert_file,
                ssl_client_cert=cert_file,
                ssl_client_key=cert_file,
            )
        )
        try:
            plugin_fixture._populate_rpm_data(repo_url)
            assert CollectRpmPlugin.rpm_data[str(repo_url)] == xmltodict.parse(
                xml_input=TEST_PRIMARY_XML, force_list=["package"]
            )

            # Run a second time to assert idempotence
            plugin_fixture._populate_rpm_data(repo_url)
            assert len(CollectRpmPlugin.rpm_data) == 1
        except HopprPluginError as ex:
            assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
            id="stream-success",
        ),
        pytest.param(
            "Failed to retrieve data from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/repomd.xml",
            {"status_code": 404},
            id="stream-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test_stream_url_data(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._stream_url_data method."""
    repomd_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/repomd.xml"

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        response = plugin_fixture._stream_url_data(url=RepositoryUrl(url=repomd_url))
        assert response.url == repomd_url
    except requests.HTTPError as ex:
        assert str(ex) == expected_exception  # noqa: PT017


@pytest.mark.parametrize(
    argnames=["pkg_url", "response_fixture", "expected_result"],
    argvalues=[
        pytest.param(
            {"version": "7.8.9-1.fc37"},
            {},
            Result.fail(
                message=(
                    "Failure after 3 attempts, final message RPM package not found in repository: "
                    "'pkg:rpm/fedora/hoppr-test-rpm@7.8.9-1.fc37?arch=x86_64&distro=fedora-37&epoch=0'"
                )
            ),
            id="rpm-data-not-found",
        ),
        pytest.param(
            {},
            {"content": b"download fail", "status_code": 404},
            Result.fail(message="HTTP Status Code: 404; download fail"),
            id="rpm-download-fail",
        ),
        pytest.param(
            {},
            {
                "status_code": 200,
                "content": b'{"urls": [{"digests": {"blake2b_256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                b'"md5": "a0a32b159feca49e7b13b9a49ae0127ade587f8b",'
                b'"sha256": "a0a32b159feca49e7b13b9a49ae0127ade587f8b"},"filename": "TEST.whl"},{"digests": '
                b'{"blake2b_256": "9dbe10918a2eac4ae9f02f6cfe6414b7a155ccd8f7f9d4380d62fd5b955065c3",'
                b'"md5": "941e175c276cd7d39d098092c56679a4",'
                b'"sha256": "942c5a758f98d790eaed1a29cb6eefc7ffb0d1cf7af05c3d2791656dbd6ad1e1"},'
                b'"filename": "TEST.tar.gz"}]}',
            },
            Result.success(
                message="",
                return_obj=Component(
                    name="hoppr-test-rpm",
                    version="1.2.3-1.fc37",
                    purl="pkg:rpm/fedora/hoppr-test-rpm@1.2.3-1.fc37?arch=x86_64&distro=fedora-37&epoch=0",
                    type=ComponentType.LIBRARY,
                ),
            ),
            id="collect-success",
        ),
    ],
    indirect=["pkg_url", "response_fixture"],
)
def test_collect(
    expected_result: Result,
    component: Component,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
    tmp_path: Path,
):
    """Test CollectRpmPlugin.collect method."""
    repo_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setattr(target=CollectRpmPlugin, name="_get_repo_files", value=lambda x: True)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo_url,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    (tmp_path / "test.pem").write_text("Test")
    plugin_fixture.repo_configs.append(
        RepoConfig(
            base_url=["test.com", "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"],
            ssl_client_cert=(tmp_path / "test.pem"),
            ssl_client_key=(tmp_path / "test.pem"),
        )
    )

    result = plugin_fixture.collect(component, repo_url)

    assert result.status == expected_result.status
    assert result.message == expected_result.message
    assert result.return_obj == expected_result.return_obj


def test_pre_stage_process(
    plugin_fixture: CollectRpmPlugin, sbom: Sbom, monkeypatch: pytest.MonkeyPatch, tmp_path: Path
):
    """Test CollectRpmPlugin.pre_stage_process method."""
    rpm_data = xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"])
    plugin_fixture.context.strict_repos = False

    def _populate_rpm_data_patch(repo_url: RepositoryUrl):
        assert str(repo_url) in {
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
            "testurl.com",
            "testurl2.com",
        }

        CollectRpmPlugin.rpm_data[str(repo_url)] = rpm_data

    monkeypatch.setattr(target=plugin_fixture.context, name="consolidated_sbom", value=sbom)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_rpm_data", value=_populate_rpm_data_patch)

    (tmp_path / "test.txt").write_text("Test file for cert/key.")
    Path.mkdir(Path("/", "etc", "yum.repos.d"), exist_ok=True)
    Path("/", "etc", "yum.repos.d", "test.repo").write_text(f"""[test_repo]
baseurl = testurl.com
enabled = 1
gpgcheck = 1
gpgkey = testgpg
name = Test Repo
priority = 1
proxy = _none_
username = username
password = password
ssl_client_cert = {tmp_path / "test.txt"}
ssl_client_key = {tmp_path / "test.txt"}""")
    Path("/", "etc", "yum.repos.d", "test2.repo").write_text("""[test_repo]
baseurl = testurl2.com
enabled = 1
gpgcheck = 1
gpgkey = testgpg
name = Test Repo
priority = 1
proxy = _none_
username = username
password = password""")

    result = plugin_fixture.pre_stage_process()

    assert result.is_success()
    assert plugin_fixture.rpm_data["https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"] == rpm_data
    assert plugin_fixture.rpm_data["https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"] == rpm_data


@pytest.mark.parametrize(
    argnames=["should_fail", "mock_hash"],
    argvalues=[
        pytest.param(
            False,
            Hash(alg=cdx.HashAlg.SHA_512, content="ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a"),
            id="success",
        ),
        pytest.param(
            True,
            Hash(alg=cdx.HashAlg.SHA_512, content="ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171b"),
            id="failure",
        ),
    ],
)
def test_validate_hashes(
    plugin_fixture: CollectRpmPlugin,
    component: Component,
    should_fail: bool,
    mock_hash: Hash,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin_validate_hashes method."""
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[
            Hash(alg=cdx.HashAlg.SHA_512, content="ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a")
        ],
    )
    if should_fail:
        with pytest.raises(HopprPluginError):
            plugin_fixture._validate_hashes(mock_hash, component)
    else:
        assert plugin_fixture._validate_hashes(mock_hash, component) is None


@pytest.mark.parametrize(
    argnames=["package_path", "mock_generated_hash", "expected_exception"],
    argvalues=[
        pytest.param(Path("some/path/to/file"), "a0a32b159feca49e7b13b9a49ae0127ade587f8b", None, id="success"),
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8c",
            "Hash for hoppr-test-rpm does not match expected hash.",
            id="failure",
        ),
    ],
)
def test_update_hashes(
    package_path: Path,
    mock_generated_hash: str,
    expected_exception: str,
    plugin_fixture: CollectRpmPlugin,
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectRpmPlugin._update_hashes method."""
    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[Hash(alg=cdx.HashAlg.SHA_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
    )
    monkeypatch.setattr(target=hoppr.net, name="get_file_hash", value=lambda x, y: mock_generated_hash)

    try:
        plugin_fixture._update_hashes(component, package_path)
    except (HopprPluginError, ValueError) as ex:
        assert str(ex) == expected_exception  # noqa: PT017


def test__file_exists(plugin_fixture: CollectRpmPlugin, tmp_path: Path):
    tmp_path = tmp_path / "test"
    (tmp_path).write_text("Test")
    assert _file_exists(tmp_path) == tmp_path
    (tmp_path).unlink()
    with pytest.raises(ValueError):  # noqa: PT011
        _file_exists(tmp_path)


@pytest.mark.parametrize(
    argnames=["input", "expected_output"],
    argvalues=[
        pytest.param("$basearch", "test_arch"),
        pytest.param("$releasever", "test_ver"),
        pytest.param("$variable", "parsed_variable"),
        pytest.param("$variable-$basearch-$releasever", "parsed_variable-test_arch-test_ver"),
    ],
)
def test__match_dnf_var(
    tmp_path: Path,
    input: str,
    expected_output: str,
):
    RepoConfig.base_arch = "test_arch"
    RepoConfig.release_ver = "test_ver"
    RepoConfig.vars_dir = tmp_path
    (RepoConfig.vars_dir / "variable").write_text("parsed_variable")
    assert RepoConfig._match_dnf_var(input) == expected_output


@pytest.mark.parametrize(
    argnames=["values", "expected"],
    argvalues=[
        pytest.param({"key1": "value1"}, {"key1": "value1"}),
        pytest.param({"key1": "$var"}, {"key1": "variable"}),
        pytest.param({"key3": ["1", "$var", "3"]}, {"key3": ["1", "variable", "3"]}),
    ],
)
def test__resolve_dnf_vars(
    values: dict[str, str | list[str]],
    expected: dict[str, str | list[str]],
    tmp_path: Path,
):
    """Test _resolve_dnf_vars function."""
    RepoConfig.vars_dir = tmp_path
    (tmp_path / "var").write_text("variable")

    result = RepoConfig._resolve_dnf_vars(values)

    assert result == expected


@pytest.mark.parametrize(
    argnames=["hash_alg", "hash_string", "expected"],
    argvalues=[
        pytest.param("md", "a" * 32, "md5", id="md5"),
        pytest.param("sha", "a" * 40, "sha1", id="sha1"),
        pytest.param("sha", "a" * 56, "sha224", id="sha224"),
        pytest.param("sha", "a" * 64, "sha256", id="sha256"),
        pytest.param("sha", "a" * 96, "sha384", id="sha384"),
        pytest.param("sha", "a" * 128, "sha512", id="sha512"),
    ],
)
def test__determine_sha_hash_alg(plugin_fixture: CollectRpmPlugin, hash_alg: str, hash_string: str, expected: str):
    """Test the _determine_sha_hash_alg type."""
    assert plugin_fixture._determine_sha_hash_alg(hash_alg, hash_string) == expected
