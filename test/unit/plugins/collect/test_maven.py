"""Test module for CollectMavenPlugin class."""

from __future__ import annotations

import importlib
import sys

from collections.abc import Callable
from pathlib import Path

import pytest
import requests

import hoppr.plugin_utils

from hoppr import Hash, cdx, net
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.plugins.collect.maven import CollectMavenPlugin
from hoppr.result import Result

SETTINGS_XML_EXPERIMENTAL = """<settings>
    <profiles>
        <profile>
            <id>hoppr-maven-test-profile</id>
            <repositories>
                <repository>
                    <id>hoppr-test--maven-repo-1</id>
                    <name>Hoppr Maven Test 1</name>
                    <url>https://nowhere.com</url>
                </repository>
                <repository>
                    <id>hoppr-test-maven-repo-2</id>
                    <name>Hoppr Maven Test 2</name>
                    <url>https://nowhere.else.com</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
"""


@pytest.fixture(name="component")
def component_fixture(request: pytest.FixtureRequest):
    """Test Component fixture."""
    purl = getattr(request, "param", "pkg:maven/something/else@1.2.3")
    return Component(name="TestMavenComponent", purl=purl, type="file")  # type: ignore[arg-type]


@pytest.fixture
def context_fixture(context_fixture: HopprContext, monkeypatch: pytest.MonkeyPatch) -> HopprContext:
    """Test Context fixture."""
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML_EXPERIMENTAL)

    return context_fixture


@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectMavenPlugin,
    monkeypatch: pytest.MonkeyPatch,
    tmp_path: Path,
) -> CollectMavenPlugin:
    """Override and parametrize plugin_fixture to return CollectMavenPlugin."""
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.config = {"maven_command": "mvn", "maven_opts": ["-D1", "-D2"]}
    plugin_fixture.context.repositories[PurlType.MAVEN] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_fixture", "expected_result"],
    argvalues=[
        pytest.param({"strict_repos": False}, {"status_code": 200}, "SUCCESS", id="OK (200)"),
        pytest.param({"strict_repos": False}, {"status_code": 401}, "FAIL", id="Unauthorized (401)"),
        pytest.param({"strict_repos": False}, {"status_code": 403}, "FAIL", id="Forbidden (403)"),
        pytest.param({"strict_repos": False}, {"status_code": 404}, "FAIL", id="Not Found (404)"),
        pytest.param({"strict_repos": False}, {"status_code": 500}, "FAIL", id="Internal Server Error (500)"),
        pytest.param({"strict_repos": False}, {"status_code": 502}, "FAIL", id="Bad Gateway (502)"),
    ],
    indirect=["context_fixture", "response_fixture"],
)
def test_collect_maven_experimental(
    context_fixture: HopprContext,
    component: Component,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
    expected_result: str,
):
    """Test CollectMavenPlugin.collect method (experimental)."""
    monkeypatch.setattr(target=net, name="download_file", value=response_fixture)
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.plugins.collect.maven")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=module.CollectMavenPlugin,
    )

    monkeypatch.setattr(target=CollectMavenPlugin, name="_check_artifact_hash", value=lambda *_, **__: Result.success())
    monkeypatch.setattr(target=CollectMavenPlugin, name="update_hashes", value=lambda *_, **__: Result.success())

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    collect_result = plugin.process_component(component)

    assert collect_result.status.name == expected_result


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_generator"],
    argvalues=[pytest.param({"strict_repos": False}, ["response_200", "response_404", "response_200", "response_404"])],
    indirect=True,
)
def test_collect_maven_experimental_download_pom_fail(
    context_fixture: HopprContext,
    response_generator: Callable[..., requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectMavenPlugin.collect method (experimental) pom file download failure."""
    monkeypatch.setattr(target=net, name="download_file", value=response_generator)
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.plugins.collect.maven")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=module.CollectMavenPlugin,
    )

    monkeypatch.setattr(target=CollectMavenPlugin, name="_check_artifact_hash", value=lambda *_, **__: Result.success())

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    monkeypatch.setattr(target=plugin, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    collect_result = plugin.process_component(component)

    assert collect_result.is_fail()
    assert collect_result.message.startswith(f"Failed to download pom for Maven artifact {component.purl}")


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_fixture"],
    argvalues=[({"strict_repos": False}, {"status_code": 200})],
    indirect=True,
)
def test_maven_experimental__check_artifact_hash(
    context_fixture: HopprContext,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectMavenPlugin._check_artifact_hash method (experimental)."""
    monkeypatch.setattr(target=net, name="download_file", value=response_fixture)
    monkeypatch.setattr(target=net, name="get_file_hash", value=lambda *_, **__: "")
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.plugins.collect.maven")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=module.CollectMavenPlugin,
    )

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    check_hash_result = plugin._check_artifact_hash("https://somewhere.com/test-artifact.jar", "test-artifact.jar")

    assert check_hash_result.is_fail()
    assert check_hash_result.message == (
        "HTTP Status Code: 200\nSHA1 hash for test-artifact.jar does not match expected hash."
    )


@pytest.mark.parametrize(
    argnames=["package_path", "mock_generated_hash", "expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8b",
            None,
            {"status_code": 200},
            id="success",
        ),
        pytest.param(
            Path("some/path/to/file"),
            "a0a32b159feca49e7b13b9a49ae0127ade587f8c",
            "Hash for TestMavenComponent does not match expected hash.",
            {"status_code": 200},
            id="failure",
        ),
    ],
)
def test_update_hashes(
    package_path: Path,
    mock_generated_hash: str,
    expected_exception: str,
    context_fixture: HopprContext,
    response_fixture: Callable[[str], requests.Response],
    component: Component,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test CollectMavenPlugin.update_hashes method."""
    monkeypatch.setattr(target=net, name="download_file", value=response_fixture)
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.plugins.collect.maven")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=module.CollectMavenPlugin,
    )

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    monkeypatch.setattr(
        target=component,
        name="hashes",
        value=[Hash(alg=cdx.HashAlg.SHA_512, content="a0a32b159feca49e7b13b9a49ae0127ade587f8b")],
    )
    monkeypatch.setattr(target=net, name="get_file_hash", value=lambda x, y: mock_generated_hash)

    try:
        plugin.update_hashes(component, package_path)
    except (HopprPluginError, ValueError) as ex:
        assert str(ex) == expected_exception  # noqa: PT017
