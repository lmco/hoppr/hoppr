"""Test module for base `hopctl` commands."""

from __future__ import annotations

import re
import sys

import pytest

from typer.testing import CliRunner

from hoppr.cli import app


@pytest.mark.parametrize(argnames="is_tty", argvalues=[True, False], ids=["tty", "not_tty"])
def test_version(hopctl_runner: CliRunner, is_tty: bool, monkeypatch: pytest.MonkeyPatch):
    """Test `hopctl version` output."""
    with monkeypatch.context() as patch:
        patch.setattr(target=sys.stdout, name="isatty", value=lambda: is_tty)

        result = hopctl_runner.invoke(app, args="version", color=True)
        pattern = re.compile(
            pattern=r"^.+Hoppr Framework Version.*:.*Python Version.*:.*$",
            flags=re.DOTALL,
        )
        matched = pattern.search(string=result.stdout)

        assert matched is not None
