"""Test module for `hopctl validate` subcommand with '--experimental' tag."""

from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING

import pytest

import hoppr.cli.experimental.validate
import hoppr.cli.options
import hoppr.models.validation
import hoppr.net
import hoppr.utils

from hoppr.cli import app
from hoppr.models.validation.checks import ValidateConfig
from hoppr.models.validation.code_climate import IssueList

if TYPE_CHECKING:
    from typer.testing import CliRunner


@pytest.mark.parametrize(
    argnames="sbom_id",
    argvalues=[
        pytest.param("expiration", id="expiration"),
        pytest.param("last-renewal", id="last-renewal"),
        pytest.param("licenses-field", id="licenses-field"),
        pytest.param("licenses-types", id="licenses-types"),
        pytest.param("metadata-authors", id="metadata-authors"),
        pytest.param("metadata-supplier", id="metadata-supplier"),
        pytest.param("metadata-timestamp", id="metadata-timestamp"),
        pytest.param("name-field", id="name-field"),
        pytest.param("name-or-id", id="name-or-id"),
        pytest.param("purchase-order", id="purchase-order"),
        pytest.param("sbom-components", id="sbom-components"),
        pytest.param("sbom-spec-version", id="sbom-spec-version"),
        pytest.param("sbom-unique-id", id="sbom-unique-id"),
        pytest.param("sbom-vulnerabilities-field", id="sbom-vulnerabilities-field"),
        pytest.param("supplier-field", id="supplier-field"),
        pytest.param("unique-id", id="unique-id"),
        pytest.param("valid", id="valid"),
        pytest.param("version-field", id="version-field"),
    ],
)
def test_hopctl_validate_sbom_experimental(
    hopctl_runner: CliRunner,
    sbom_id: str,
    resources_dir: Path,
    monkeypatch: pytest.MonkeyPatch,
    tmp_path: Path,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` option."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / f"expected-issues-{sbom_id}.json")

    args = [
        "validate",
        "sbom",
        f"--log={tmp_path / 'hoppr-pytest.log'}",
        f"--sbom={sbom_file}",
        f"--output-file={tmp_path / 'results.json'}",
        "--experimental",
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)

        result = hopctl_runner.invoke(app, args=args)

        assert result.exit_code == 0
        assert IssueList.parse_file(tmp_path / "results.json") == expected

    # Add `--profile strict` and assert failing exit code
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)

        args += ["--profile", "strict"]
        result = hopctl_runner.invoke(app, args=args)

        assert result.exit_code == 0 if sbom_id == "valid" else 1


def test_hopctl_validate_sbom_experimental_full_terminal(
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` option."""
    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: False)
    monkeypatch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)

    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / "validate-test-valid.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / "expected-issues-valid.json")

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom={sbom_file}",
            f"--output-file={tmp_path / 'results.json'}",
            "--experimental",
        ],
    )

    assert result.exit_code == 0
    assert IssueList.parse_file(tmp_path / "results.json") == expected


def test_hopctl_validate_sbom_experimental_sbom_urls(
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` and `--sbom-url` options."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / "validate-test-valid.cdx.json"

    def _download_file_patch(dest: str, *_, **__):
        content = sbom_file.read_text(encoding="utf-8")
        Path(dest).write_text(data=content, encoding="utf-8")

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)
        patch.setattr(target=hoppr.net, name="download_file", value=_download_file_patch)

        result = hopctl_runner.invoke(
            app,
            args=[
                "validate",
                "sbom",
                f"--log={tmp_path / 'hoppr-pytest.log'}",
                "--sbom-url=https://example.acme.com/sboms/sbom.cdx.json",
                f"--output-file={tmp_path / 'results.json'}",
                "--experimental",
            ],
        )

        assert result.exit_code == 0


@pytest.mark.parametrize(
    argnames=["sbom_id", "expected_log"],
    argvalues=[
        pytest.param(
            "components",
            [
                "Validating Metadata",
                "Issues found for license Acme Software License:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Validating component: Acme Software@1.0.0",
                "Missing field: 'supplier'",
                "Missing field: 'licenses'",
                "Validating component: sub-component",
                "Issues found for license MIT:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Missing field: 'supplier'",
                "Missing field: 'version'",
                "Missing fields: one of 'cpe', 'purl', 'swid'",
                "Missing field: 'supplier'",
                "Validating component: debian@12.2",
                "Missing field: 'supplier'",
                "Missing field: 'licenses'",
                "Missing fields: one of 'cpe', 'purl', 'swid'",
                "Validating component: adduser@3.134",
                "Issues found for license GPL-2.0:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Validating component: apt@2.6.1",
                "Issues found for license GPL-2.0:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Issues found for license BSD-3-Clause:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Issues found for license Expat:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Missing field: 'vulnerabilities'",
            ],
        ),
        pytest.param(
            "metadata-timestamp",
            [
                "Validating Metadata",
                "Missing field: 'timestamp'",
                "Validating component: Acme Software@1.0.0",
                "Validating component: sub-component@1.0.0",
                "Missing fields: one of 'cpe', 'purl', 'swid'",
                "Validating component: debian@12.2",
                "Missing fields: one of 'cpe', 'purl', 'swid'",
                "Validating component: github.com/alecthomas/participle/v2@v2.0.0",
                "Issues found for license MIT:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Validating component: github.com/alecthomas/participle/v2@2.0.0",
                "Issues found for license MIT:",
                "Missing or invalid lastRenewal",
                "Missing or invalid licenseTypes",
                "Missing or invalid purchaseOrder",
                "Validating component: adduser@3.134",
                "Validating component: apt@2.6.1",
            ],
        ),
    ],
    ids=["components", "metadata-timestamp"],
)
def test_hopctl_validate_sbom_experimental_logging_validation_error(
    hopctl_runner: CliRunner,
    sbom_id: str,
    expected_log: list[str],
    tmp_path: Path,
    resources_dir: Path,
    caplog: pytest.LogCaptureFixture,
    monkeypatch: pytest.MonkeyPatch,
):
    """Test logging output of `hopctl validate sbom` with `--experimental` option if a pydantic `ValidationError` is raised."""
    # Set logger back to None so a new one gets created
    monkeypatch.setattr(target=hoppr.cli.options, name="_logger", value=None)

    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom={sbom_file}",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
            "--verbose",
            "--experimental",
        ],
        catch_exceptions=False,
    )

    actual_log = [message.lstrip() for message in caplog.messages]
    expected_log.insert(0, f"Validating {sbom_file}...")

    assert result.exit_code == 0
    assert actual_log == expected_log


def test_experimental_validate__get_issues_dict():
    """Test `hoppr.cli.experimental.validate._get_issues_dict` method with an issue missing its check_name."""
    issue_list = IssueList.parse_obj([
        {
            "description": "Dummy issue",
            "location": {
                "path": "",
                "positions": {
                    "begin": {"line": 0, "column": 0},
                    "end": {"line": 0, "column": 0},
                },
            },
            "severity": "info",
        }
    ])

    issues_dict = hoppr.cli.experimental.validate._get_issues_dict(issue_list)

    assert all(len(value) == 0 for value in issues_dict.values())
