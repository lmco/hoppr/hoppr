{
  "title": "HopprSchemaModel",
  "description": "Consolidated Hoppr config file schema definition.",
  "discriminator": {
    "propertyName": "kind",
    "mapping": {
      "Credentials": "#/definitions/CredentialsFile",
      "Manifest": "#/definitions/ManifestFile",
      "Transfer": "#/definitions/TransferFile"
    }
  },
  "oneOf": [
    {
      "$ref": "#/definitions/CredentialsFile"
    },
    {
      "$ref": "#/definitions/ManifestFile"
    },
    {
      "$ref": "#/definitions/TransferFile"
    }
  ],
  "additionalProperties": false,
  "definitions": {
    "HopprMetadata": {
      "title": "HopprMetadata",
      "description": "Metadata data model.",
      "type": "object",
      "properties": {
        "name": {
          "title": "Name",
          "type": "string"
        },
        "version": {
          "title": "Version",
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "integer"
            }
          ]
        },
        "description": {
          "title": "Description",
          "type": "string"
        }
      },
      "required": [
        "name",
        "version",
        "description"
      ]
    },
    "CredentialRequiredService": {
      "title": "CredentialRequiredService",
      "description": "CredentialRequiredService data model.",
      "type": "object",
      "properties": {
        "url": {
          "title": "Url",
          "type": "string"
        },
        "user_env": {
          "title": "User Env",
          "type": "string"
        },
        "pass_env": {
          "title": "Pass Env",
          "type": "string"
        },
        "user": {
          "title": "User",
          "type": "string"
        },
        "password": {
          "title": "Password",
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "string",
              "writeOnly": true,
              "format": "password"
            }
          ]
        }
      },
      "required": [
        "url",
        "pass_env",
        "user"
      ],
      "additionalProperties": false
    },
    "CredentialsFile": {
      "title": "CredentialsFile",
      "description": "Credentials file data model.",
      "type": "object",
      "properties": {
        "kind": {
          "title": "Kind",
          "enum": [
            "Credentials"
          ],
          "type": "string"
        },
        "metadata": {
          "title": "Metadata",
          "description": "Metadata for the file",
          "allOf": [
            {
              "$ref": "#/definitions/HopprMetadata"
            }
          ]
        },
        "schemaVersion": {
          "title": "Schema Version",
          "default": "v1",
          "type": "string"
        },
        "credential_required_services": {
          "title": "Credential Required Services",
          "description": "List of CredentialRequiredService objects to provide authentication to remote repositories and/or registries",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/CredentialRequiredService"
          }
        }
      },
      "required": [
        "kind"
      ],
      "additionalProperties": false
    },
    "LocalFile": {
      "title": "LocalFile",
      "description": "LocalFile data model.",
      "type": "object",
      "properties": {
        "local": {
          "title": "Local",
          "type": "string",
          "format": "path"
        }
      },
      "required": [
        "local"
      ],
      "additionalProperties": false
    },
    "OciFile": {
      "title": "OciFile",
      "description": "OciFile data model.",
      "type": "object",
      "properties": {
        "oci": {
          "title": "Oci",
          "anyOf": [
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string"
            }
          ]
        }
      },
      "required": [
        "oci"
      ],
      "additionalProperties": false
    },
    "UrlFile": {
      "title": "UrlFile",
      "description": "UrlFile data model.",
      "type": "object",
      "properties": {
        "url": {
          "title": "Url",
          "anyOf": [
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 2083,
              "format": "uri"
            },
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string",
              "minLength": 1,
              "maxLength": 65536,
              "format": "uri"
            },
            {
              "type": "string"
            }
          ]
        }
      },
      "required": [
        "url"
      ],
      "additionalProperties": false
    },
    "Repository": {
      "title": "Repository",
      "description": "Repository data model.",
      "type": "object",
      "properties": {
        "url": {
          "title": "Url",
          "pattern": "^([^:/?#]+:(?=//))?(//)?(([^:]+(?::[^@]+?)?@)?[^@/?#:]*(?::\\d+?)?)?[^?#]*(\\?[^#]*)?(#.*)?",
          "type": "string"
        },
        "description": {
          "title": "Description",
          "type": "string"
        }
      },
      "required": [
        "url"
      ],
      "additionalProperties": false
    },
    "Repositories": {
      "title": "Repositories",
      "description": "Repositories data model.",
      "type": "object",
      "properties": {
        "cargo": {
          "title": "Cargo",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "deb": {
          "title": "Deb",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "docker": {
          "title": "Docker",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "gem": {
          "title": "Gem",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "generic": {
          "title": "Generic",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "git": {
          "title": "Git",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "github": {
          "title": "Github",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "gitlab": {
          "title": "Gitlab",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "golang": {
          "title": "Golang",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "helm": {
          "title": "Helm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "maven": {
          "title": "Maven",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "npm": {
          "title": "Npm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "nuget": {
          "title": "Nuget",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "oci": {
          "title": "Oci",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "pypi": {
          "title": "Pypi",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "raw": {
          "title": "Raw",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "repo": {
          "title": "Repo",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        },
        "rpm": {
          "title": "Rpm",
          "default": [],
          "uniqueItems": true,
          "type": "array",
          "items": {
            "$ref": "#/definitions/Repository"
          }
        }
      },
      "additionalProperties": false
    },
    "ManifestFile": {
      "title": "ManifestFile",
      "description": "Data model to describe a single manifest file.",
      "type": "object",
      "properties": {
        "kind": {
          "title": "Kind",
          "enum": [
            "Manifest"
          ],
          "type": "string"
        },
        "metadata": {
          "title": "Metadata",
          "description": "Metadata for the file",
          "allOf": [
            {
              "$ref": "#/definitions/HopprMetadata"
            }
          ]
        },
        "schemaVersion": {
          "title": "Schema Version",
          "default": "v1",
          "type": "string"
        },
        "sboms": {
          "title": "Sboms",
          "description": "List of references to local or remote SBOM files",
          "default": [],
          "type": "array",
          "items": {
            "anyOf": [
              {
                "$ref": "#/definitions/LocalFile"
              },
              {
                "$ref": "#/definitions/OciFile"
              },
              {
                "$ref": "#/definitions/UrlFile"
              }
            ]
          }
        },
        "repositories": {
          "title": "Repositories",
          "description": "Maps supported PURL types to package repositories/registries",
          "allOf": [
            {
              "$ref": "#/definitions/Repositories"
            }
          ]
        },
        "includes": {
          "title": "Includes",
          "description": "List of manifest files to load",
          "default": [],
          "type": "array",
          "items": {
            "anyOf": [
              {
                "$ref": "#/definitions/LocalFile"
              },
              {
                "$ref": "#/definitions/UrlFile"
              }
            ]
          }
        }
      },
      "required": [
        "kind",
        "repositories"
      ],
      "additionalProperties": false
    },
    "Plugin": {
      "title": "Plugin",
      "description": "Plugin data model.",
      "type": "object",
      "properties": {
        "name": {
          "title": "Name",
          "description": "Name of plugin",
          "type": "string"
        },
        "config": {
          "title": "Config",
          "description": "Mapping of additional plugin configuration settings to values",
          "type": "object"
        }
      },
      "required": [
        "name"
      ],
      "additionalProperties": false
    },
    "Stage": {
      "title": "Stage",
      "description": "Stage data model.",
      "type": "object",
      "properties": {
        "component_coverage": {
          "title": "Component Coverage",
          "description": "Defines how often components should be processed",
          "enum": [
            "AT_LEAST_ONCE",
            "EXACTLY_ONCE",
            "NO_MORE_THAN_ONCE",
            "OPTIONAL"
          ],
          "type": "string"
        },
        "plugins": {
          "title": "Plugins",
          "description": "List of Hoppr plugins to load",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Plugin"
          }
        }
      },
      "required": [
        "plugins"
      ],
      "additionalProperties": false
    },
    "TransferFile": {
      "title": "TransferFile",
      "description": "Transfer file data model.",
      "type": "object",
      "properties": {
        "kind": {
          "title": "Kind",
          "enum": [
            "Transfer"
          ],
          "type": "string"
        },
        "metadata": {
          "title": "Metadata",
          "description": "Metadata for the file",
          "allOf": [
            {
              "$ref": "#/definitions/HopprMetadata"
            }
          ]
        },
        "schemaVersion": {
          "title": "Schema Version",
          "default": "v1",
          "type": "string"
        },
        "max_processes": {
          "title": "Max Processes",
          "description": "Max processes to create when running Hoppr application",
          "type": "integer"
        },
        "stages": {
          "title": "Stages",
          "description": "Mapping of stage names to property definitions",
          "type": "object",
          "additionalProperties": {
            "$ref": "#/definitions/Stage"
          }
        }
      },
      "required": [
        "kind",
        "stages"
      ],
      "additionalProperties": false
    }
  }
}