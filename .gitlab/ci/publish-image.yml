---
skopeo:copy:
  stage: publish
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  needs:
    - job: build-image:release
    - job: semantic-release:dry-run
    - job: test-bundle
  script:
    - if [ -z ${RELEASE_VERSION} ]; then echo "Version not set" && exit 0; else echo "Version is set to '$RELEASE_VERSION'"; fi
    - skopeo login --username $USER --password $TOKEN $REGISTRY
    - skopeo copy --format oci "docker://$CI_REGISTRY_IMAGE/hopctl:$CI_COMMIT_REF_SLUG" "docker://$IMAGE:$VERSION"
  parallel:
    matrix:
      - NAME: Copy to Gitlab
        IMAGE: $CI_REGISTRY_IMAGE/hopctl
        REGISTRY: $CI_REGISTRY
        USER: gitlab-ci-token
        TOKEN: $GITLAB_TOKEN
        VERSION: $RELEASE_VERSION

      - NAME: Copy to DockerHub
        IMAGE: hoppr/hopctl
        REGISTRY: index.docker.io
        USER: $DOCKER_USER
        TOKEN: $DOCKER_TOKEN
        VERSION: $RELEASE_VERSION

      - NAME: Copy latest to DockerHub
        IMAGE: hoppr/hopctl
        REGISTRY: index.docker.io
        USER: $DOCKER_USER
        TOKEN: $DOCKER_TOKEN
        VERSION: latest
  rules:
    - if: $CI_COMMIT_REF_NAME == "main"

sign-image:
  stage: publish
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  needs:
    - job: skopeo:copy
    - job: semantic-release:dry-run
  before_script:
    - if [ -z ${RELEASE_VERSION} ]; then echo "Version not set" && exit 0; else echo "Version is set to '$RELEASE_VERSION'"; fi
    # Get the secret sauce
    - dnf install --quiet --assumeyes jq libxcrypt-compat which

    # Install Trivy
    - dnf install --quiet --assumeyes https://github.com/aquasecurity/trivy/releases/download/v0.45.1/trivy_0.45.1_Linux-64bit.rpm

    # Install Cosign
    - cosign_rpm="https://github.com/sigstore/cosign/releases/download/v2.2.0/cosign-2.2.0.x86_64.rpm"
    - dnf install --quiet --assumeyes "$cosign_rpm"

    - skopeo login --username $USER --password $TOKEN $REGISTRY
    - cosign login --username $USER --password $TOKEN $REGISTRY
  script:
    - digest="$(skopeo inspect --format {{.Digest}} "docker://$IMAGE:$RELEASE_VERSION")"

    # Generate SBOM
    - trivy image --security-checks vuln --output $CI_PROJECT_NAME-$digest.cdx.json --timeout 15m0s --debug --format cyclonedx $IMAGE:$RELEASE_VERSION
    - cosign attest --predicate $CI_PROJECT_NAME-$digest.cdx.json --type cyclonedx $IMAGE:$RELEASE_VERSION
    - cosign sign $IMAGE@$digest

    # Must be named cosign.pub for cosign to use it for verification
    - echo "Waiting for rekor to store signature" && sleep 30s
    - cosign verify $IMAGE@$digest --certificate-identity=https://gitlab.com/hoppr/hoppr//.gitlab-ci.yml@refs/heads/main --certificate-oidc-issuer=https://gitlab.com
  parallel:
    matrix:
      - IMAGE: $CI_REGISTRY_IMAGE/hopctl
        REGISTRY: $CI_REGISTRY
        USER: gitlab-ci-token
        TOKEN: $GITLAB_TOKEN

      - IMAGE: hoppr/hopctl
        REGISTRY: index.docker.io
        USER: $DOCKER_USER
        TOKEN: $DOCKER_TOKEN
  artifacts:
    reports:
      cyclonedx:
        - $CI_PROJECT_NAME-*.cdx.json
    paths:
      - $CI_PROJECT_NAME-*.cdx.json
      - cosign.pub
      - digest.env
  rules:
    - if: $CI_COMMIT_REF_NAME == "main"
