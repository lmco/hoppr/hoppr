<!--
Thank you for your merge request. Please provide a description above and review
the requirements below.

Bug fixes and new features should include tests and possibly benchmarks.
Contributors guide: ./docs/CONTRIBUTING.md

All MRs must successfully pass continuous integration checks.
- `poetry run ruff format`
- `poetry run ruff check`
- `poetry run pytest`
- `poetry run mypy`
- `poetry run pytest`
-->

### Checklist
<!-- Remove items that do not apply. For completed items, change [ ] to [x]. -->

- [ ] documentation is changed or added in `./docs`
- [ ] unit tests updated to test changes

<!-- NOTE: these things are not required to open a MR and can be done afterwards / while the MR is open. -->

### Description

#### Context
<!-- Please describe what event led to this MR. Links to incidents, meetings, conversations. -->

#### Intent
<!-- Please describe what you intend to accomplish with this MR, such as design decisions. -->
